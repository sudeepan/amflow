Changes since v1.1
==================

Bug fixes
---------
 - Fixed a bug of differential equation solver, which might result in overflows when computing asymptotic series.
 - Fixed a bug of "Mass" mode, which might introduce eta to unexpected propagators.
 - Fixed a bug of Kira interface, which might result in unexpected terminations for the mpi version of Kira.
 - Fixed a bug of defining cache directories, which would cause privilege violation when using interactive command line.

New features
------------
  - Users can now use AMFlow on Windows operation system, with IBP reducers installed in WSL (or the Windows Subsystem for Linux). Users need to set correct paths in install.m for each reducer. 
  - Users can now use Blade as the integration-by-parts reducer, by SetReductionOptions["IBPReducer" -> "Blade"], after a proper installation. See README.md for more details.
 - Users can now perform the calculations with fixed epsilon value, by including a numerical replacement in AMFlowInfo["Numeric"], e.g., AMFlowInfo["Numeric"] = {s -> 1, t-> -100, eps -> 11/4514};
 - Users can now compute target integrals with multiple top-sectors.

Other changes
-------------
 - Optimized the order of variables (including kinematic variables and epsilon) when reducing integrals with FiniteFlow+LiteRed, which may reduce the number of sample points.


v1.1
====

Bug fixes
---------
 - Fixed a bug of kira interface, which might result in failure of numerical reduction.

New features
------------
 - Users can now use FIRE+LiteRed as the integration-by-parts reducer, by SetReductionOptions["IBPReducer" -> "FIRE+LiteRed"], after a proper installation. See README.md for more details.
 - Users no longer need to define AMFlowInfo["Cut"] when dealing with loop integrals.
 - Users can now define Feynman prescriptions for each loop when dealing with phase-space integrations by AMFlowInfo["Prescription"] = {1, -1, ...}. See examples/feynman_prescription/run.wl for more details.
 - Users can now define their own preference to insert eta by SetAMFOptions["AMFMode" -> {"keyword1", "keyword2", ...}]. See examples/user_defined_amfmode/run.wl for more details.
 - Users can now define their own preference for ending systems and input their own boundary integrals for them by SetAMFOptions["EndingScheme" -> {"keyword1", "keyword2", ...}]. See examples/user_defined_ending/run.wl for more details.
 - Users can now compute epsilon-expansion of integrals with any D = D0-2eps using 'SolveIntegrals' by setting SetAMFOptions["D0" -> D0]. See examples/spacetime_dimension/run.wl for more details.
 - Users can now compute integrals at complex kinematic points. See examples/complex_kinematics/run.wl for more details.
 - Users can now determine whether to turn on or turn off the function of finding symmetries among external momenta when using "FiniteFlow+LiteRed" by SetReducerOptions["EMSymmetry" -> True] or SetReducerOptions["EMSymmetry" -> False]. By default, this function is turned off.
 - The function 'SolveIntegrals' can now output vanishing target integrals.
 - The function 'BlackBoxAMFlow' can now compute any target integrals, while the old version can only deal with master integrals.

Other changes
-------------
 - Optimized the organization of temporary files.
 - Optimized the generation of boundary integrals.
 - Optimized the support for MacOS.
 - Changed the default value of "AMFMode" in SetAMFOptions to {"Prescription", "Mass", "Propagator"}.
 - Changed the default value of "DeleteBlackBoxDirectory" in SetReductionOptions to False.
 - Changed the names of many internal functions.


v1.0
====
 - Released.
