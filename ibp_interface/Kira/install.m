(* ::Package:: *)

(*For Unix:*)


(*
(*kira executable, version 2.2 or later*)
$KiraExecutable = "/usr/local/src/kira/kira-2.3";
(*fermat executable*)
$FermatExecutable = "/usr/local/src/ferl6/fer64";
*)


(*For Window with WSL*)



(*kira executable, version 2.2 or later*)
$KiraExecutable = "/home/octaquark4/Softwares/HEPTools/Corrections/IBP/Kira/Install/bin/kira";
(*fermat executable*)
$FermatExecutable = "/home/octaquark4/Softwares/HEPTools/Corrections/HepLib/Install/ferl6/fer64";

