(* ::Package:: *)

(* ::Subsubsection:: *)
(*begin*)


BeginPackage["Blade`", {"AMFlow`"}];


$BladePath::usage = "path to Blade.wl.";


Begin["`Private`"];


(*Enable the use of WSL within Windows operation system*)
wslQ:=StringMatchQ[$BladePath,"\\\\wsl.localhost\\"~~___];
wsl:=If[wslQ,"wsl",""];
(*In Windows, WSL files are in the director: "\\\\wsl.localhost\\Ubuntu\\..."*)
pathChange:=If[wslQ,"/"<>FileNameJoin[FileNameSplit[#][[2;;-1]],OperatingSystem->"Unix"]&,Identity];
(*In WSL, Windows files "X:\\..." is in the directory: "/mnt/x/..."*)
fileNameJoin:=If[wslQ,
	StringReplace[FileNameJoin[{#[[1]]//FileNameSplit,#[[2;;-1]]}//Flatten,OperatingSystem->"Unix"],
		c_~~":":>"/mnt/"<>ToLowerCase[c]]&,
	FileNameJoin
];
bladePath:=FileNameJoin[{$BladePath,"Blade.wl"}]//pathChange;


$WolframPath = First[$CommandLine];
$Current = DirectoryName[$InputFileName];
$CTX = $Context;


Family := AMFlowInfo["Family"];
Loop := AMFlowInfo["Loop"];
IndepLeg := Select[AMFlowInfo["Leg"], !MemberQ[Keys[AMFlowInfo["Conservation"]], #]&];
SPToSTU:= Module[{complete,keys,values,bare,full},
If[IndepLeg === {}, Return[{}]];
complete = Outer[Times, IndepLeg, IndepLeg]//Flatten//DeleteDuplicates;
keys = Expand[Keys[AMFlowInfo["Replacement"]]/.AMFlowInfo["Conservation"]];
values = Values[AMFlowInfo["Replacement"]];
bare = Coefficient[#, complete]&/@keys;
full = Append[Transpose[bare],values]//Transpose;
If[MatrixRank[bare]<Length[complete], Print["SPToSTU: insufficient replacement rules for all independent external scalar products."]; Abort[]];
If[MatrixRank[full]>Length[complete], Print["SPToSTU: inconsistent replacement rules."]; Abort[]];
Thread[complete -> RowReduce[full][[;;Length[complete], -1]]]
];
Propagator := AMFlowInfo["Propagator"]/.AMFlowInfo["Conservation"];
Cut := If[Head[AMFlowInfo["Cut"]]===List, AMFlowInfo["Cut"], ConstantArray[0, Length[Propagator]]];
Prescription := If[Head[AMFlowInfo["Prescription"]]===List, AMFlowInfo["Prescription"], ConstantArray[1, Length[Loop]]];
IBPRule := If[!TrueQ[ComplexMode], AMFlowInfo["Numeric"], Select[AMFlowInfo["Numeric"], Im[#[[2]]]===0&]];
CompensateRule := Complement[AMFlowInfo["Numeric"], IBPRule];
NThread := AMFlowInfo["NThread"];


$AuxLeg:=Symbol[ToStringInput[Family]<>"AuxLeg"];
Leg:=Append[IndepLeg, $AuxLeg];
Conservation:={$AuxLeg -> -Total[IndepLeg]};


STUToSP:=Block[{right, var, col, mat, group},
right = Values[SPToSTU];
var = Variables[right];
If[var==={}, Return[{}]];
{col, mat} = CoefficientArrays[right, var]//Normal;
group = MaximalGroup[mat];
Solve[Thread[Keys[SPToSTU]==Values[SPToSTU]][[group]], var][[1]]//Expand];


Momentum:=ToSquareAll[Propagator][[1]];
Mass:=ToSquareAll[Propagator][[2]];


MassScale:=Join[Variables[Values[SPToSTU]], Complement[Variables[Propagator], Loop, IndepLeg]]//DeleteDuplicates;


BladeIntegrals[jint_]:=Symbol["Global`BL"][Family, FromJ[jint]];


(* ::Subsubsection::Closed:: *)
(*topology*)


TopologyTemplate[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"family = `fam`;
dimension = 4-2eps;
loop = `loop`;
leg = `leg`;
conservation = `cons`;
replacement = `rep`;
propagator = `prop`;
topsector = `top`;
numeric = `num`;
BLNthreads = `nth`;
cut = `cut`;
pres = `pres`;
BLFamilyDefine[family, dimension, propagator, loop, leg, conservation, replacement, topsector, numeric, cut, pres];"
}];

rule = <|
"fam" -> ToStringInput[Family],
"loop" -> ToStringInput[Loop],
"leg" -> ToStringInput[Leg],
"cons" -> ToStringInput[Conservation],
"rep" -> ToStringInput[SPToSTU],
"prop" -> ToStringInput[Propagator],
"top" -> ToStringInput[TopSector],
"num" -> ToStringInput[IBPRule],
"cut" -> ToStringInput[Cut],
"pres" -> ToStringInput[Prescription],
"nth" -> ToStringInput[NThread]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "topology.wl"}]];
];


(* ::Subsubsection::Closed:: *)
(*reduction-analytic*)


AnalyticReductionTemplate[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[`blade`];
Get[\"topology.wl\"];",

"target = Get[\"target\"];
preferred = Get[\"preferred\"];",

"amfint[blint_]:=j[blint[[1]], Sequence@@blint[[2]]];
res = BLReduce[target, preferred]/.BLFamilyInf[family][\"MastersRules\"];
masters = Cases[res, _BL, Infinity]//DeleteDuplicates;
res = Coefficient[#, masters]&/@res;
Put[{amfint/@masters, Thread[amfint/@target -> res]}, FileNameJoin[{current, \"results/table\"}]];",
"Quit[];"
}];

rule = <|"blade" -> ToStringInput[bladePath]|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "reductionAna.wl"}]];
];


(* ::Subsubsection:: *)
(*reduction-diffeq*)


DifferentialEquationTemplate[inv_,dir_]:=Module[{template,rule},
template = StringToTemplate[{
"current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[`blade`];
Get[\"topology.wl\"];",

"preferred = Get[\"preferred\"];",

"amfint[blint_]:=j[blint[[1]], Sequence@@blint[[2]]];
{masters, invs, diffeq} = BLDifferentialEquation[`vars`, preferred];
Put[{amfint/@masters, invs, diffeq}, FileNameJoin[{current, \"results/diffeq\"}]];",
"Quit[];"
}];
rule = <|"blade" -> ToStringInput[bladePath],
"vars" -> ToStringInput[inv]|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "reductionDe.wl"}]];
];


(* ::Subsubsection:: *)
(*usage*)


CheckDep[]:=Module[{},
Print["CheckDep: dependencies of current reducer:"];
Print["Blade.m" -> FileExistsQ[$BladePath]];
];


Options[SetReducerOptions] = {};
SetReducerOptions[opt___]:=Block[{},
PrintOptions[SetReducerOptions, $CTX];
];


IBPSystem[top_,rank_,dot_,preferred_,complexmode_,dir_]:=Module[{},
CheckCompleteness[];
$ReductionDirectory = dir;
DeleteDir[dir];
CreateDir[dir];
TopSector = If[#===_, 1, 0]&/@top;
IBPRank = rank;
IBPDot = dot;
ComplexMode = complexmode;
TopologyTemplate[dir];
PutFile[BladeIntegrals/@preferred, FileNameJoin[{dir, "preferred"}]];
Print[StringTemplate["IBPSystem: generating ibp system with rank `1` and dot `2`."][ToStringInput[IBPRank], ToStringInput[IBPDot]]];
Print[StringTemplate["IBPSystem: ibp system generated in `1`s."][ToStringInput[0]]];
];


AnalyticReduction[target_]:=Module[{dir, time, res},
dir = $ReductionDirectory;
AnalyticReductionTemplate[dir];
PutFile[BladeIntegrals/@target, FileNameJoin[{dir, "target"}]];
CreateDir[FileNameJoin[{dir, "results"}]];
Print[StringTemplate["AnalyticReduction: reducing `1` target integrals."][ToStringInput[Length[target]]]];
time = RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "reductionAna.wl"}], "log" -> FileNameJoin[{dir, "reductionAna.log"}]];
res = GetFile[FileNameJoin[{$ReductionDirectory, "results/table"}]];
Print[StringTemplate["AnalyticReduction: target integrals reduced to `1` master integrals in `2`s."][ToStringInput[Length[res[[1]]]], ToStringInput[Ceiling[time]]]];
{res[[1]], Thread[Keys[res[[2]]] -> (Values[res[[2]]]/.CompensateRule)]}
];


DifferentialEquation[inv_]:=Module[{dir, time, res},
dir = $ReductionDirectory;
DifferentialEquationTemplate[inv, dir];
CreateDir[FileNameJoin[{dir, "results"}]];
Print[StringTemplate["DifferentialEquation: constructing differential equations with respect to `1`."][ToStringInput[inv]]];
time = RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "reductionDe.wl"}], "log" -> FileNameJoin[{dir, "reductionDe.log"}]];
res = GetFile[FileNameJoin[{$ReductionDirectory, "results/diffeq"}]];
Print[StringTemplate["DifferentialEquation: differential equations constructed with `1` master integrals in `2`s."][ToStringInput[Length[res[[1]]]], ToStringInput[Ceiling[time]]]];
{res[[1]], res[[2]], res[[3]]/.CompensateRule}
];


(* ::Subsubsection::Closed:: *)
(*end*)


GetFile[FileNameJoin[{$Current, "install.m"}]];
CheckDep[];
SetReducerOptions@@Options[SetReducerOptions];


End[];


EndPackage[];
