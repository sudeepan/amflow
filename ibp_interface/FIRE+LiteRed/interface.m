(* ::Package:: *)

(* ::Subsubsection:: *)
(*begin*)


BeginPackage["FIRE`", {"AMFlow`"}];


$FIREInstallationDirectory::usage = "FIRE installation directory.";
$FIREVersion::usage = "FIRE version.";


Begin["`Private`"];


(*Enable the use of WSL within Windows operation system*)
wslQ:=StringMatchQ[$FIREInstallationDirectory,"\\\\wsl.localhost\\"~~___];
wsl:=If[wslQ,"wsl",""];
(*In Windows, WSL files are in the director: "\\\\wsl.localhost\\Ubuntu\\..."*)
pathChange:=If[wslQ,"/"<>FileNameJoin[FileNameSplit[#][[2;;-1]],OperatingSystem->"Unix"]&,Identity];
(*In WSL, Windows files "X:\\..." is in the directory: "/mnt/x/..."*)
fileNameJoin:=If[wslQ,
	StringReplace[FileNameJoin[{#[[1]]//FileNameSplit,#[[2;;-1]]}//Flatten,OperatingSystem->"Unix"],
		c_~~":":>"/mnt/"<>ToLowerCase[c]]&,
	FileNameJoin
];

firePath:=$FIREPath//pathChange;
fireExecutable:=$FIREExecutable//pathChange;
liteRedPath:=$LiteRedPath//pathChange;


$FIREPath := FileNameJoin[{$FIREInstallationDirectory, "FIRE"<>ToStringInput[$FIREVersion]<>".m"}];
$FIREExecutable := FileNameJoin[{$FIREInstallationDirectory, "bin", "FIRE"<>ToStringInput[$FIREVersion]}];
$LiteRedPath := FileNameJoin[{$FIREInstallationDirectory, "extra", "LiteRed", "Setup"}];


$WolframPath = First[$CommandLine];
$CTX = $Context;
ep = Symbol["Global`eps"];


Family := AMFlowInfo["Family"];
Loop := AMFlowInfo["Loop"];
IndepLeg := Select[AMFlowInfo["Leg"], !MemberQ[Keys[AMFlowInfo["Conservation"]], #]&];
SPToSTU:= Module[{complete,keys,values,bare,full},
If[IndepLeg === {}, Return[{}]];
complete = Outer[Times, IndepLeg, IndepLeg]//Flatten//DeleteDuplicates;
keys = Expand[Keys[AMFlowInfo["Replacement"]]/.AMFlowInfo["Conservation"]];
values = Values[AMFlowInfo["Replacement"]];
bare = Coefficient[#, complete]&/@keys;
full = Append[Transpose[bare],values]//Transpose;
If[MatrixRank[bare]<Length[complete], Print["SPToSTU: insufficient replacement rules for all independent external scalar products."]; Abort[]];
If[MatrixRank[full]>Length[complete], Print["SPToSTU: inconsistent replacement rules."]; Abort[]];
Thread[complete -> RowReduce[full][[;;Length[complete], -1]]]
]/.IBPRule;
Propagator := AMFlowInfo["Propagator"]/.AMFlowInfo["Conservation"];
Cut := If[Head[AMFlowInfo["Cut"]]===List, AMFlowInfo["Cut"], ConstantArray[0, Length[Propagator]]];
IBPRule := If[!TrueQ[ComplexMode], AMFlowInfo["Numeric"], Select[AMFlowInfo["Numeric"], Im[#[[2]]]===0&]];
CompensateRule := Complement[AMFlowInfo["Numeric"], IBPRule];
NThread := AMFlowInfo["NThread"];


$AuxLeg:=Symbol[ToStringInput[Family]<>"AuxLeg"];
Leg:=If[IndepLeg=!={}, Append[IndepLeg, $AuxLeg], {}];
Conservation:=If[IndepLeg=!={}, {$AuxLeg -> -Total[IndepLeg]}, {{}}];


STUToSP:=Block[{right, var, col, mat, group},
right = Values[SPToSTU];
var = Variables[right];
If[var==={}, Return[{}]];
{col, mat} = CoefficientArrays[right, var]//Normal;
group = MaximalGroup[mat];
Solve[Thread[Keys[SPToSTU]==Values[SPToSTU]][[group]], var][[1]]//Expand];


Momentum:=ToSquareAll[Propagator][[1]]/.IBPRule;
Mass:=ToSquareAll[Propagator][[2]]/.IBPRule;


MassScale:=Join[Variables[Values[SPToSTU]], Complement[Join[Variables[Momentum], Variables[Mass]], Loop, IndepLeg]]//DeleteDuplicates;


IBPParameter:=Symbol["x"<>ToString[#]]&/@Range[Length[MassScale]];


(* ::Subsubsection::Closed:: *)
(*FIRE*)


ListToString[list_]:=StringJoin@Riffle[StringReplace[ToStringInput/@list, " " -> ""], ", "];


FrobeniusSolve2[{},0]:={{}};
FrobeniusSolve2[{},b_]:={};
FrobeniusSolve2[{a_},b_]:={{b/a}};
FrobeniusSolve2[a_, b_]:=FrobeniusSolve[a,b];


JLower[jint_]:=ToJ/@Tuples[If[#<=0,{0},Range[0,#]]&/@FromJ[jint]];
JLowerSector[jint_]:=ToJ/@Tuples[If[#<=0,{0},{0,#}]&/@FromJ[jint]];


JSectorDown[jsec_,n_]:=Module[{power,posi,sol,tmp},
power = FromJ[jsec];
posi = Flatten[Position[power, 0]];
sol = -FrobeniusSolve2[ConstantArray[1, Length[posi]], n];
Table[tmp = power; tmp[[posi]] = sol[[i]]; ToJ[tmp],{i,Length[sol]}]];
JSectorDownCollect[jsec_,n_]:=Join@@(JSectorDown[jsec,#]&/@Reverse[Range[0,n]]);


TopologyDefinition[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"Internal = `loop`;
External = `leg`;
Propagators = `props`;
Replacements = `rep`;
topsec = `topsec`;
cut = `cut`;"}];

rule = <|
"loop" -> ToStringInput[Loop],
"leg" -> ToStringInput[IndepLeg],
"props" -> ToStringInput[Expand[Momentum^2+Mass]/.Thread[MassScale -> IBPParameter]],
"rep" -> ToStringInput[SPToSTU/.Thread[MassScale -> IBPParameter]],
"topsec" -> ToStringInput[TopSector],
"cut" -> ToStringInput[Cut]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "topology.wl"}]];
];


FIREStart[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"If[$FrontEnd===Null, $InputFileName, NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`FIRE`];
Get[\"topology.wl\"];
PrepareIBP[];
Prepare[AutoDetectRestrictions -> False, Parallel -> False];
SaveStart[ToString[`fam`]];
Quit[];"}];

rule = <|
"FIRE" -> ToStringInput[firePath],
"fam" -> ToStringInput[Family]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "FIRE_start.wl"}]];
];


LiteRedStart[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"If[$FrontEnd===Null, $InputFileName, NotebookFileName[]]//DirectoryName//SetDirectory;
AppendTo[$Path, `litered`];
Get[\"LiteRed.m\"];
Get[`FIRE`];
Get[\"topology.wl\"];
CreateNewBasis[`fam`, Directory -> \"litered\"];
GenerateIBP[`fam`];
AnalyzeSectors[`fam`, topsec, CutDs -> cut];
FindSymmetries[`fam`, EMs -> `emsym`];
DiskSave[`fam`];
Quit[];"}];

rule = <|
"litered" -> ToStringInput[liteRedPath],
"FIRE" -> ToStringInput[firePath],
"fam" -> ToStringInput[Family],
"emsym" -> ToStringInput[$EMSymmetry]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "litered_start.wl"}]];
];


LiteRedCXX[dir_]:=Module[{template,rule},
template = StringToTemplate[{
"If[$FrontEnd===Null, $InputFileName, NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`FIRE`];
LoadStart[ToString[`fam`], 0];
TransformRules[\"litered\", ToString[`fam`]<>\".lbases\", 0];
SaveSBases[ToString[`fam`]];
Quit[];"}];

rule = <|
"FIRE" -> ToStringInput[firePath],
"fam" -> ToStringInput[Family]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "litered_cxx.wl"}]];
];


FIRECXX[dir_, mode_]:=Module[{template,rule},
template = StringToTemplate[{
"#threads `ibpkernel`
#variables `variables`
#start
#problem 0 `sbases`
#lbases `lbases`
#preferred preferred
#integrals target",
If[mode==="Master", "#masters `out`", 
"#rules `rule`
#output `out`"]}];
rule = <|
"ibpkernel" -> ToStringInput[NThread],
"variables" -> ListToString[Prepend[IBPParameter, If[MemberQ[Keys[IBPRule], ep], Symbol["d"] -> (4-2*ep/.IBPRule), Symbol["d"]]]],
"sbases" -> ToStringInput[Family]<>".sbases",
"lbases" -> ToStringInput[Family]<>".lbases",
"out" -> ToStringInput[Family]<>".tables",
"rule" -> ToStringInput[Family]<>".rules"
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, ToStringInput[Family]<>".config"}]];
];


FIREResult[dir_, mode_]:=Module[{template,rule},
template = StringToTemplate[{
"If[$FrontEnd===Null, $InputFileName, NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`FIRE`];
Get[\"topology.wl\"];
If[!FileExistsQ[ToString[`fam`]<>\".tables\"], Print[\"error: reduction tables not found.\"]; Abort[]];",
If[mode==="Master", 
"LoadStart[ToString[`fam`], 0];
Burn[];
LoadTables[ToString[`fam`]<>\".tables\"];
rules = FindRules[MasterIntegrals[]];
SaveRulesToFile[rules, ToString[`fam`]];
masters = j[`fam`, Sequence@@#]&/@Complement[MasterIntegrals[][[All,2]], Keys[rules][[All,2]]];
Put[masters, \"results/masters_mma\"];",
"masters = Get[\"results/masters_mma\"];
rules = Tables2Rules[ToString[`fam`]<>\".tables\", Together, True];
rules = rules/.G[0, a_]:>j[`fam`, Sequence@@a];
rules = Join[rules, # -> #&/@masters];
ints = Cases[Values[rules], _j, Infinity]//DeleteDuplicates;
If[!SubsetQ[masters, ints], Print[\"error: integrals reduced to non-masters.\"]; Abort[]];
rules = Thread[Keys[rules] -> (Coefficient[#, masters]&/@Values[rules]/.d -> 4-2eps//Together)];
Put[{masters, rules}, \"results/table\"];"],
"Quit[];"}];

rule = <|
"FIRE" -> ToStringInput[firePath],
"fam" -> ToStringInput[Family]
|>;

FileTemplateApply[template, rule, FileNameJoin[{dir, "FIRE_result.wl"}]];
];


(* ::Subsubsection::Closed:: *)
(**compute derivatives (code from LiteIBP.m provided by T.Peraro)*)


Clear[mm];


(* scalar product *)
SetAttributes[mp,Orderless];
mp[p1_Plus, p2_] := mp[#, p2] & /@ p1;
mp[p2_, coeff__ mm[p1__]] := coeff mp[mm[p1], p2];
mp[0,p_]:=0;
mp2[p_] := mp[p, p];

ExpandMP[expr_]:= expr/. {mp[a_,b_]:>mp[Expand[a,mm],Expand[b,mm]]};


(* shorthands *)
mmp[a_,b_]:=mp[mm[a],mm[b]];
mmp2[a_]:=mp[mm[a],mm[a]];


(* metric tensors in D and 4 dimensions *)
SetAttributes[gD, Orderless];
SetAttributes[g4, Orderless];


LContract[expr_,mpD_,mp4_]:= (Expand[expr]/. {gD[mm[k1_],mm[k2_]]:>mp[mm[k1],mm[k2]],
											  gD[mm[k_],mm[k_]]:>mp[mm[k],mm[k]],
											  g4[mm[k1_],mm[k2_]]:>mp[mm[k1],mm[k2]],
	                                           g4[mm[k_],mm[k_]]:>mp[mm[k],mm[k]]}) //. {
	gD[mm[k_],mu_]:>mm[k][mu],
	gD[mu_,mu_] :> MetricD,
	g4[mm[k_],mu_]:>mm[k][mu],
	g4[mu_,mu_] :> 4,
	gD[mu_,nu_]^2 :> MetricD,
    gD[mu_,nu_]gD[nu_,sigma_] :> gD[mu,sigma],
    g4[mu_,nu_]g4[nu_,sigma_] :> g4[mu,sigma],
    gD[mu_,nu_]g4[nu_,sigma_] :> g4[mu,sigma],
	g4[mu_,nu_]^2 :> 4,
	mm[k_][mu_]^2 :> mp[mm[k],mm[k]],
	mm[k1_][mu_] mm[k2_][mu_] :> mp[mm[k1],mm[k2]],
	gD[mu_,nu_] mm[k1_][mu_] mm[k2_][nu_] :> mpD[mm[k1],mm[k2]],
	g4[mu_,nu_] mm[k1_][mu_] mm[k2_][nu_] :> mp4[mm[k1],mm[k2]]}
LContract[expr_]:=LContract[expr,mp,mp] //. {gD[mu_,nu_] mm[k1_][mu_] :> mm[k1][nu],
											 g4[mu_,nu_] mm[k1_][mu_] :> mm[k1][nu]}


(* Derivatives w.r.t. mmenta *)
MomDerivative[expr_,mm[q_][mu_],dim_,gmetric_,finalcontraction_]:=
 ((D[expr /. {mm[q][nu_] :> IndexedMomentum[mm[q], nu]},mm[q]]/.
	{Derivative[1, 0][mp][a_, b_] :> b[mu],
	 Derivative[0, 1][mp][a_, b_] :> a[mu],
     Derivative[1, 0][IndexedMomentum][mm[q], mu] :> dim,  
     Derivative[1, 0][IndexedMomentum][mm[q], nu_] :> gmetric[mu,nu]}) /.
    {IndexedMomentum[mm[q],nu_] :> mm[q][nu]} ) // finalcontraction
MomDerivative[expr_,mm[q_][mu_]]:=MomDerivative[expr,mm[q][mu],MetricD,gD,LContract];


IndexedMomExpr[expr_Plus,mu_]:=IndexedMomExpr[#,mu]&/@expr;
IndexedMomExpr[coeff__ mm[a__],mu_]:=coeff IndexedMomExpr[mm[a],mu];
IndexedMomExpr[mm[a__],mu_]:=mm[a][mu];


LIBPLoopMomenta[fam_]:=Loop;
LIBPIndepExtMomenta[fam_]:=IndepLeg;
SPRule[fam_]:=Join@@Table[If[i===j,Leg[[i]]Leg[[j]]->mmp2[Leg[[i]]],Leg[[i]]Leg[[j]]->mmp[Leg[[i]],Leg[[j]]]],{i,Length@Leg},{j,i,Length@Leg}];
Props[fam_]:=mp2/@(Momentum/.Thread[Join[Loop,Leg]->mm/@Join[Loop,Leg]])+Mass/.LIBPIds[fam]//Expand;
LIBPIds[fam_]:=Thread[(SPToSTU[[All,1]]/.SPRule[fam])->SPToSTU[[All,2]]];
LIBPInvariants[fam_]:=Thread[STUToSP[[All,1]]->(Expand@STUToSP[[All,2]]/.SPRule[fam])];
LIBPDenoms[fam_]:=Table[Symbol["j"][fam, Sequence@@(-UnitVector[Length[Momentum],i])]->Together[Props[fam][[i]]/.LIBPIds[fam]],{i,1,Length[Momentum]}];


LIBPSps[fam_]:= Join[DeleteDuplicates[Flatten[Outer[mmp[#1,#2]&,LIBPLoopMomenta[fam],LIBPLoopMomenta[fam]]]],
                     Flatten[Outer[mmp[#1,#2]&,LIBPLoopMomenta[fam],LIBPIndepExtMomenta[fam]]]];


LIBPSpsToJ[fam_]:=LIBPSpsToJ[fam,Sequence@@ConstantArray[1,Length[LIBPDenoms[fam]]]];
LIBPSpsToJ[fam_,dens___]:=Module[{densidx,thisdens},
  densidx = Flatten[Position[{dens},1]];
  thisdens = LIBPDenoms[fam][[densidx]];
  Solve[(#[[2]]-#[[1]]==0)&/@thisdens,LIBPSps[fam]][[1]]
];


LIBPGetDerivatives[topo_]:=Module[
  {invs, invl, ids, pi,derivs,eqs,vars,pij,inveqs,mysys,learn,mat},
  invs = LIBPInvariants[topo];
  invl = #[[1]]&/@invs;
  ids = LIBPIds[topo];
  pi = LIBPIndepExtMomenta[topo];
  derivs = Table[-mm[LIBPDerivV[pp]][mu]+ mm[pp][mu]LIBPDerivV[mmp2[pp]]+Sum[mm[qq][mu]LIBPDerivV[mmp[pp,qq]],{qq,pi}],{pp,pi}];
  eqs=Collect[Flatten[Table[LContract[mm[pp][mu] ddd],{ddd,derivs},{pp,pi}]],_LIBPDerivV,Expand[#/.ids]&];
  pij = Flatten[Table[mmp[pi[[i]],pi[[j]]],{i,Length[pi]},{j,i,Length[pi]}]];
  inveqs = Table[LIBPDerivV[xk]-Sum[D[ppij/.ids,xk] LIBPDerivV[ppij],{ppij,pij}],{xk,invl}];
  vars=Join[LIBPDerivV/@pij,Flatten[Table[mmp[LIBPDerivV[pp],qq],{pp,pi},{qq,pi}]]];
  
  mat = CoefficientArrays[(#==0)&/@Join[inveqs,eqs], Join[LIBPDerivV/@invl,vars]][[2]]//Normal;
  mat = RowReduce[mat]//Factor;
  mat = Select[mat, FirstPosition[#,1][[1]] <= Length[invl]&];
  Solve[Thread[mat . Join[LIBPDerivV/@invl,vars] == 0], LIBPDerivV/@invl][[1]]
];


LIBPDerivivative[topo_,expr_,inv_,derivs_]:=Module[{mu},
  Together[((LIBPDerivV[inv]/.derivs)/.mp[mm[a_],mm[LIBPDerivV[b_]]]:>LContract[mm[a][mu]MomDerivative[expr/.LIBPInvariants[topo],mm[b][mu]]])/.LIBPIds[topo]]
];


LIBPComputeDerivatives[topo_]:=Module[{},
ClearAll[LIBPDenomsDeriv];
If[LIBPInvariants[topo]=!={},
LIBPDerivatives[topo]=LIBPGetDerivatives[topo];
Table[LIBPDenomsDeriv[topo,sss]=(Collect[LIBPDerivivative[topo,#,sss,LIBPDerivatives[topo]]/.LIBPSpsToJ[topo],_?(Head[#]===Symbol["j"]&),Together]&/@(#[[2]]&/@LIBPDenoms[topo]));,{sss,First/@LIBPInvariants[topo]}];
];
LIBPDenomsDeriv[topo,s_]:=LIBPDenomsDeriv[topo,s]=Collect[D[#,s]/.LIBPSpsToJ[topo],_?(Head[#]===Symbol["j"]&),Together]&/@Values[LIBPDenoms[topo]]; 
];


LIBPDeriv[j_[t_Symbol,a__],s_]:=LIBPDenomsDeriv[t,s] . ((-{a}[[#]] j[t,Sequence@@(UnitVector[Length[{a}],#]+{a})])&/@Range[Length[{a}]]);


LIBPDeriv[a_Plus,s_]:=LIBPDeriv[#,s]&/@a;
LIBPDeriv[a_Times,s_]:=Plus@@(MapAt[LIBPDeriv[#,s]&,a,#]&/@Range[Length[a]]);
LIBPDeriv[expr_,s_]:=D[expr,s];


ComputeDerivative[target_, x_]:=Collect[Expand[LIBPDeriv[#,x]&/@target]//. Symbol["j"][a_, b___] * Symbol["j"][a_, b2___] :> Symbol["j"][a, Sequence@@({b}+{b2})],_?(Head[#]===Symbol["j"]&),Together];


(* ::Subsubsection:: *)
(*usage*)


CheckDep[]:=Module[{},
Print["CheckDep: dependencies of current reducer:"];
Print["FIRE"<>ToStringInput[$FIREVersion]<>".m" -> FileExistsQ[$FIREPath]];
Print["FIRE"<>ToStringInput[$FIREVersion]<>" executable" -> FileExistsQ[$FIREExecutable]];
Print["LiteRed.m" -> FileExistsQ[FileNameJoin[{$LiteRedPath, "LiteRed.m"}]]];
];


Options[SetReducerOptions] = {"EMSymmetry" -> False};
SetReducerOptions[opt___]:=Block[{},
If[MemberQ[Keys[{opt}], "EMSymmetry"], $EMSymmetry = "EMSymmetry"/.{opt}];

PrintOptions[SetReducerOptions, $CTX];
];


IBPSystem[top_,rank_,dot_,preferred_,complexmode_,dir_]:=Block[{time,target},
CheckCompleteness[];
$ReductionDirectory = dir;
DeleteDir[dir];
CreateDir[dir];
TopSector = top;
IBPRank = rank;
IBPDot = dot;
ComplexMode = complexmode;
TopologyDefinition[dir];
FIREStart[dir];
LiteRedStart[dir];
LiteRedCXX[dir];
FIRECXX[dir, "Master"];
FIREResult[dir, "Master"];
(*probe for masters*)
target = Flatten[JSectorDownCollect[#, IBPRank]&/@(JLowerSector@ToJ[If[#===_,1,0]&/@TopSector])];
PutFile[{0, FromJ[#]}&/@preferred, FileNameJoin[{dir, "preferred"}]];
PutFile[{0, FromJ[#]}&/@target, FileNameJoin[{dir, "target"}]];
CreateDir[FileNameJoin[{dir, "results"}]];
time = 0;
Print[StringTemplate["IBPSystem: generating ibp system with rank `1` and dot `2`."][ToStringInput[IBPRank], ToStringInput[IBPDot]]];
time = time + RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "FIRE_start.wl"}], "log" -> FileNameJoin[{dir, "FIRE_start.log"}]];
time = time + RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "litered_start.wl"}], "log" -> FileNameJoin[{dir, "litered_start.log"}]];
time = time + RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "litered_cxx.wl"}], "log" -> FileNameJoin[{dir, "litered_cxx.log"}]];
time = time + RunCommand[wsl<>fireExecutable<>" -c "<>fileNameJoin[{dir, ToStringInput[Family]}], "log" -> FileNameJoin[{dir, ToStringInput[Family]<>".config.log"}], ProcessDirectory -> dir];
time = time + RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "FIRE_result.wl"}], "log" -> FileNameJoin[{dir, "FIRE_result.log"}]];
Print[StringTemplate["IBPSystem: ibp system generated in `1`s."][ToStringInput[Ceiling[time]]]];
];


AnalyticReduction[target_]:=Module[{dir,time,masters,rules},
dir = $ReductionDirectory;
PutFile[{0, FromJ[#]}&/@target, FileNameJoin[{dir, "target"}]];
FIRECXX[dir, "Reduction"];
FIREResult[dir, "Reduction"];
time = 0;
Print[StringTemplate["AnalyticReduction: reducing `1` target integrals."][ToStringInput[Length[target]]]];
time = time + RunCommand[wsl<>fireExecutable<>" -c "<>fileNameJoin[{dir, ToStringInput[Family]}], "log" -> FileNameJoin[{dir, ToStringInput[Family]<>".config.log"}], ProcessDirectory -> dir];
time = time + RunCommand[wsl<>$WolframPath<>" -noprompt -script "<>fileNameJoin[{dir, "FIRE_result.wl"}], "log" -> FileNameJoin[{dir, "FIRE_result.log"}]];
{masters, rules} = GetFile[FileNameJoin[{dir, "results/table"}]];
Print[StringTemplate["AnalyticReduction: target integrals reduced to `1` master integrals in `2`s."][ToStringInput[Length[masters]], ToStringInput[Ceiling[time]]]];
{masters, Thread[Keys[rules] -> (Values[rules]/.Thread[IBPParameter -> MassScale]/.CompensateRule)]}
];


DifferentialEquation[vars_]:=Module[{time1,time2,masters,der,integrals,masnew,rules,red,diffeq,sortmasters,pindex,perm},
Print[StringTemplate["DifferentialEquation: constructing differential equations with respect to `1`."][ToStringInput[vars]]];
time1 = AbsoluteTime[];
masters = GetFile[FileNameJoin[{$ReductionDirectory, "results", "masters_mma"}]];

LIBPComputeDerivatives[Family];
der = ComputeDerivative[masters, #]&/@vars/.IBPRule;

integrals = Cases[der,_?(Head[#]===Symbol["j"]&),Infinity]//DeleteDuplicates;
{masnew, rules} = AnalyticReduction[integrals];
If[masnew=!=masters, Print["DifferentialEquation: masters of FIRE are not well-defined."]; Abort[]];
red[___]:=ConstantArray[0, Length[masnew]];
Table[red[rules[[i,1]]] = rules[[i,2]], {i,Length[rules]}];
diffeq = Together[der/.Symbol["j"][a___]:>red[Symbol["j"][a]]];
Table[If[diffeq[[i,j]]===0,diffeq[[i,j]] = ConstantArray[0,Length@masters]],{i,Length@diffeq},{j,Length@diffeq[[i]]}];

sortmasters = SortIntegrals[masters];
sortmasters = Join@@Reverse[Reverse/@GatherBy[sortmasters, JSector]];
pindex = PositionIndex[masters];
perm = Flatten[sortmasters/.pindex];
Table[diffeq[[i]] = diffeq[[i,perm,perm]],{i,Length@diffeq}];
Put[{sortmasters, vars, diffeq}, FileNameJoin[{$ReductionDirectory, "results", "diffeq"}]];
time2 = AbsoluteTime[];
Print[StringTemplate["DifferentialEquation: differential equations constructed with `1` master integrals in `2`s."][ToStringInput[Length[sortmasters]], ToStringInput[Ceiling[time2-time1]]]];
{sortmasters, vars, diffeq/.CompensateRule}
];


(* ::Subsubsection::Closed:: *)
(*end*)


GetFile[FileNameJoin[{DirectoryName[$InputFileName], "install.m"}]];
CheckDep[];
SetReducerOptions@@Options[SetReducerOptions];


End[];


EndPackage[];
