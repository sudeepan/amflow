(* ::Package:: *)

(*FIRE installation directory*)

(*For Unix:
$FIREInstallationDirectory = "/usr/local/src/fire/FIRE6";
*)
(*For Windows with WSL:*)
$FIREInstallationDirectory = "/home/octaquark4/Softwares/HEPTools/Corrections/IBP/FIRE/FIRE6";


(*FIRE version*)
$FIREVersion = 6;
