(* ::Package:: *)

BeginPackage["FiniteFlow`"];


Begin["`Private`"];


(*to use FFParallelReconstructUnivariate instead of FFReconstructUnivariate in FiniteFlow*)
Options[FFReconstructFunction]:=Join[Options[FFAlgorithmSetReconstructionOptions],{"NThreads"->FFNThreads,"MinPrimes"->1}];
FFReconstructFunction[id_,vars_,OptionsPattern[]] := Module[
  {np,maxnp,opt,res,nthreads,tmp,thisopt},
  opt = (#[[1]]->OptionValue[#[[1]]])&/@Options[FFReconstructFunction];
  maxnp = OptionValue["MaxPrimes"];
  If[TrueQ[maxnp==Automatic], maxnp = 5];
  nthreads = OptionValue["NThreads"];
  If[TrueQ[nthreads==Automatic], nthreads = If[TrueQ[Length[vars]==1],1,FFAutomaticNThreads[]]];
  If[Length[vars]==1,
    Return[FFParallelReconstructUnivariate[id,vars,Sequence@@opt]]
  ];
  res = FFAllDegrees[id, nthreads, Sequence@@FilterRules[opt,Options[FFAllDegrees]]];
  If[TrueQ[res == $Failed], Return[$Failed]];
  np = OptionValue["MinPrimes"];
  tmp = FFMissingPoints;
  res = Catch[While[True,
    If[TrueQ[np>maxnp], Throw[tmp]];
    thisopt = Join[{"MaxPrimes"->np},FilterRules[opt,Select[Options[FFSample],FreeQ[#,"MaxPrimes"]&]]];
    FFSample[id,nthreads,Sequence@@thisopt];
    tmp = FFReconstructFromCurrentEvaluations[id,vars,nthreads,Sequence@@thisopt];
    If[!TrueQ[tmp[[0]]==List],
      Switch[tmp,
             FFMissingPrimes, np=np+1;,
             _, Throw[tmp];
      ];,
      Throw[tmp];
   ]
  ]];
  res
];


End[];


EndPackage[];


BeginPackage[ "LiteIBP`",{"LiteMomentum`","LiteRed`","Vectors`","Types`","FiniteFlow`","LiteIBPPerp`"}];


Begin[ "`Private`"];


(*to sort by complexity of sector first*)
LIBPDefaultWeight[j[fam1_,a1in__]]:=Module[{a1},
  a1 = {a1in};
  (* prefer subtopologies *)
  {-Length[a1],
   -Length[Select[a1,(#>0)&]],
   -Plus@@Select[a1,(#>0)&],
   Plus@@Select[a1,(#<0)&],
   !LIBPExtMappedQ[j[fam1,a1in]],
   !LIBPMappedQ[j[fam1,a1in]],
   -Max[Select[-a1,(#>0)&]],
   j[fam1,a1in]}
];


(*to include derivatives w.r.t. mass parameters*)
LIBPComputeDerivatives[topo_]:=Module[{},
If[LIBPInvariants[topo]=!={},
LIBPDerivatives[topo]=LIBPGetDerivatives[topo];
Table[LIBPDenomsDeriv[topo,sss]=(Collect[LIBPDerivivative[topo,#,sss,LIBPDerivatives[topo]]/.LIBPSpsToJ[topo],_j,Together]&/@(#[[2]]&/@LIBPDenoms[topo]));,{sss,First/@LIBPInvariants[topo]}];
];
LIBPDenomsDeriv[topo,s_]:=LIBPDenomsDeriv[topo,s]=Collect[D[#,s]/.LIBPSpsToJ[topo],_j,Together]&/@Values[LIBPDenoms[topo]]; 
];


End[];


EndPackage[];
