(* ::Package:: *)

(*For Unix:*)


(*
(*path to FiniteFlow.m*)
$FFPath = "/usr/local/src/fflow/finiteflow-master/mathlink";
(*path to fflowmlink.so (in Linux) or fflowmlink.dylib (in MacOS)*)
$FFLibraryPath = "/usr/local/src/fflow/finiteflow-master";
(*path to LiteIBP.m*)
$LiteIBPPath = "/usr/local/src/fflow/finiteflow-mathtools-master/packages";
(*path to LiteRed.m*)
$LiteRedPath = "/usr/local/src/LiteRed/Setup";
*)


(*For Windows with WSL:*)



(*path to FiniteFlow.m*)
$FFPath = "/home/octaquark4/Softwares/HEPTools/Corrections/IBP/FiniteFlow/finiteflow/mathlink";
(*path to fflowmlink.so (in Linux) or fflowmlink.dylib (in MacOS)*)
$FFLibraryPath = "/home/octaquark4/Softwares/HEPTools/Corrections/IBP/FiniteFlow/finiteflow";
(*path to LiteIBP.m*)
$LiteIBPPath = "/home/octaquark4/Softwares/HEPTools/Corrections/IBP/FiniteFlow/finiteflow-mathtools/packages";
(*path to LiteRed.m*)
$LiteRedPath = "/home/octaquark4/.Mathematica/Applications";

