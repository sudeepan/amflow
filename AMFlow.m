(* ::Package:: *)

(* ::Subsection:: *)
(*begin*)


BeginPackage["AMFlow`"];


(*ibp-interface functions, should be defined in ibp_interface/REDUCER/interface.m*)
SetReducerOptions::usage = "SetReducerOptions[opt] sets options for current ibp reducer.";
IBPSystem::usage = "IBPSystem[top,rank,dot,preferred,complexmode,dir] sets up ibp system, nothing returned.";
AnalyticReduction::usage = "AnalyticReduction[target] reduces target integrals, a doublet {masters, rules} is returned.";
DifferentialEquation::usage = "DifferentialEquation[inv] sets up the differential equations of master integrals with respect to invariants, a triplet {master, inv, diffeq} is returned.";


AMFlowInfo::usage = "AMFlowInfo[key] returns the global object associated with key used during auxiliary mass flow.";
DefinedKeywords::usage = "DefinedKeywords[] returns currently defined keywords of AMFlowInfo.";
ExtractGlobalVariables::usage = "ExtractGlobalVariables[] extracts currently defined global variables.";
DefineGlobalVariables::usage = "DefineGlobalVariables[config] defines global variables from config.";
UnsetGlobalVariables::usage = "UnsetGlobalVariables[keys] unsets global variables with keys.";
SaveGlobalVariables::usage = "SaveGlobalVariables[n] saves currently defined global variables with id=n.";
LoadGlobalVariables::usage = "LoadGlobalVariables[n] loads global variables with id=n.";
PrintGlobalVariables::usage = "PrintGlobalVariables[] prints current global variables.";
InitializePackage::usage = "InitializePackage[] initializes the package.";
SetAMFOptions::usage = "SetAMFOptions[opt] sets auxiliary mass flow options.";
SetReductionOptions::usage = "SetReductionOptions[opt] sets reduction options.";
PrintOptions::usage = "PrintOptions[func, ctx] prints options for a function.";


RunCommand::usage = "RunCommand[command] executes the command in a new terminal.";
GetFile::usage = "GetFile[file] gets mathematica readable file.";
PutFile::usage = "PutFile[exp, fule] puts exp to file.";
CreateDir::usage = "CreateDir[dir] creates the directory.";
DeleteDir::usage = "DeleteDir[dir] deletes the directory.";
GeneralizedTranspose::usage = "GeneralizedTranspose[matrix] gives the transpose of the matrix.";
GeneralizedMatrixRank::usage = "GeneralizedMatrixRank[matrix] gives the matrix rank of the matrix.";
GeneralizedRowReduce::usage = "GeneralizedRowReduce[matrix] gives the row-reduced form of the matrix.";
ToStringInput::usage = "ToStringInput[exp] transforms exp to string with InputForm.";
StringToTemplate::usage = "StringToTemplate[stringlist] transforms a list of string into a string template.";
SameSetQ::usage = "SameSetQ[set1, set2] tests whether the two sets are the same.";
DynamicPartition::usage = "DynamicPartition[list, part] divides the list into several parts with different length according to part.";
MaximalGroup::usage = "MaximalGroup[matrix] gives the indices of linearly independent row vectors.";
MinusString::usage = "MinusString[n] returns the string with n copies of minus sign -.";
PutFirst::usage = "PutFirst[list] returns {list} if list is not {} and {} if it is.";
GetFirst::usage = "GetFirst[list] returns the first element of list if list is not {} and {} if it is.";


ToJ::usage = "ToJ[list] generates an integral from its indices.";
FromJ::usage = "FromJ[jint] generates the indecis of an integral.";
JSector::usage = "JSector[jint] returns the sector where the integral lives.";
JProp::usage = "JProp[jint] returns the number of propagators of the integral.";
JDot::usage = "JDot[jint] returns the number of dots of the integral.";
JRank::usage = "JRank[jint] returns the rank of the integral.";
IntegralWeight::usage = "IntegralWeight[jint] returns the weight of the integral.";
SortIntegrals::usage = "SortIntegrals[list] sorts a list of integrals by their weight.";
GetTopSector::usage = "GetTopSector[jints] returns the top sector of a list of integrals.";
GetTopPosition::usage = "GetTopPosition[jints] returns the indices of the top sector.";
IntegralQ::usage = "IntegralQ[exp] returns True if exp is an integral.";
CheckIntegrals::usage = "CheckIntegrals[jints] checks whether the integrals are placed correct.";
GetSector::usage = "GetSector[jint] returns the sector (List) where the integral lives.";
SubSectorQ::usage = "SubSectorQ[sec1, sec2] judges whether sec1 is a subsector of sec2.";
TopSectorQ::usage = "TopSectorQ[sec, listofsec] judges whether sec is a top sector among a list of sectors, sec must be a member of listofsec.";
GetTopSectorList::usage = "GetTopSectorList[jlist] finds all physical top sectors.";
SplitTarget::usage = "SplitTarget[jlist] split the list of integrals into several parts, each part living in a physical top sector.";


SPList::usage = "SPList[] returns a list of complete scalar products construced by loop momenta and independent external legs.";
DListSymbol::usage = "DListSymbol[] returns a list of {Denominators[1], Denominators[2], ..., Denominators[n]}, where n is the length of SPList[].";
Denominators::usage = "Denominators is a symbol representing denominators.";
CheckCompleteness::usage = "CheckCompleteness[denominators] checks the completeness of denominators.";
SPListToDListSymbol::usage = "SPListToDListSymbol[denominators] returns a list of replacement rules which expresses SPList[] as combinations DListSymbol[].";
ToCompleteExplicit::usage = "ToCompleteExplicit[denominators] generates a complete set based on denominators.";
ToSquare::usage = "ToSquare[prop] gives the corresponding momentum and mass, such that prop==momentum^2+mass.";
ToSquareAll::usage = "ToSquareAll[props] gives the corresponding momenta and masses, such that poprs==momenta^2+masses.";
SquaredDenominators::usage = "SquaredDenominators[denominators] transforms the denominator to squared form.";


FeynmanPara::usage = "FeynmanPara[n] returns a list of Feynman parameters {FeynmanParameters[1], FeynmanParameters[2], ..., FeynmanParameters[n]}.";
FeynmanParameters::usage = "FeynmanParameters is a symbol representing Feynman parameters.";
EvaluateABC::usage = "EvaluateABC[denominators] returns ingredients for computation of graph polynomial.";
EvaluateUF::usage = "EvaluateUF[denominators] returns graph polynomial U, F0 and mass term.";
AnalyzeComponent::usage = "AnalyzeComponent[u0, f, massterm] gives basic information of the component.";
AnalyzeTopology::usage = "AnalyzeTopology[denominators] gives the basic information of the topology.";
FactorizeFamily::usage = "FactorizeFamily[denominators, intpatts] factorizes the integral family specified by denominators.";


PrescriptionOfLoop::usage = "PrescriptionOfLoop[loop] returns the prescription of loop momentum, will be one of 1, -1, or 0.";
PrescriptionOf::usage = "PrescriptionOf[prop] returns the prespription of the propagator.";
AnalyzeTopSector::usage = "AnalyzeTopSector[topposi] gives the basic information of the topsector.";
VacuumQ::usage = "VacuumQ[info0] judges whether the component corresponding to info0 is a vacuum factor.";
SingleMassQ::usage = "SingleMassQ[info0] judges whether the component corresponding to info0 is a single-mass vacuum factor.";
PhaseVolumeQ::usage = "PhaseVolumeQ[info0] judges whether the component corresponding to info0 is a phase-space volume factor.";
EndingQ::usage = "EndingQ[info0] judges whether the component corresponding to info0 is an ending.";
AllPossiblePosition::usage = "AllPossiblePosition[info0, mode] gives all possible positions to insert eta.";
AMFCandidateComponent::usage = "AMFCandidateComponent[info0, mode] returns a possible choice to insert eta.";
AMFCandidate::usage = "AMFCandidate[info, mode] returns a possible choice to insert eta.";
AMFPosition::usage = "AMFPosition[topposi, mode] returns a possible choice to insert eta.";
AMFEtaC::usage = "AMFEtaC[posi] generates the a list of coefficient of eta to be inserted to each propagator.";


BranchToLoop::usage = "BranchToLoop[branch] generates a rule which can transforms loop momenta of branches into standard ones.";
BranchScale::usage = "BranchScale[branches, patt] returns whether momenta of branches are large or small.";
FindAllRegion::usage = "FindAllRegion[topposi] gives all possible retions near the infinity.";
RegionRule::usage = "RegionRule[patt] gives a transformation rule of loop momenta with given pattern.";
ExplicitRetionRule::usage = "ExplicitRetionRule[region] returns the readable replacement rules of loop momenta for current region.";
RegionPower::usage = "RegionPower[integrals, region] gives the leading power of integrals in the given region.";
BoundaryPattern::usage = "BoundaryPattern[regionpowers] generates the actual boundary leading powers when solving differential eqautions.";
FactorSum::usage = "FactorSum[sum] simplifies a summation.";
SimplifyIntegrand::usage = "SimplifyIntegrand[integrand] simplifies a integrand.";
BoundaryIntegrands::usage = "BoundaryIntegrands[integrals,border,region] gives boundary integrands corresponding to given integrals in given given with given boundary orders. ";
ApartRationals::usage = "ApartRationals[rationls] performs partial fraction to a list of rational functions.";
LaportaIntegrals::usage = "LaportaIntegrals[term] transforms a term into Larporta's notation.";
BoundaryIntegrals::usage = "BoundaryIntegrals[completede, integrands] generates boundary integrals.";
ReduceBoundary::usage = "ReduceBoundary[integrals, border, region, dir] reduces boundary integrals.";


Vacuum::usage = "Vacuum[m,n] returns the results of m-loop n-propagator single-mass vacuum integral.";


$AMFDirectory::usage = "$AMFDirectory represents the working directory for auxiliary mass flow.";
AMFSystemDirectory::usage = "AMFSystemDirectory[sysid] returns the working directory for system with sysid.";
AMFSystemNextID::usage = "AMFSystemNextID[] returns id of next system in $AMFDirectory.";
AMFSystemPathTo::usage = "AMFSystemPathTo[sysid, keyword] returns the file path corresponding to the keyword for the system.";
AMFSystemPath::usage = "AMFSystemPath[keyword] returns the default relative file path corresponding to the keyword.";
AMFSystemWrite::usage = "AMFSystemWrite[sysid, keyword, exp] writes the expressions into the file specified by AMFSystemPathTo[sysid, keyword].";
AMFSystemRead::usage = "AMFSystemRead[sysid, keyword] reads the expressions in the file specified by AMFSystemPathTo[sysid, keyword].";


AMFSystemInitialize::usage = "AMFSystemInitialize[sysid, preferred, etac] writes the necessary information of the system into files.";
AMFSystemDifferentialEquation::usage = "AMFSystemDifferentialEquation[sysid] sets up the differential equation for the system.";
AMFSystemBoundaryOrder::usage = "AMFSystemBoundaryOrder[sysid] computes the boundary orders needed to solve the differential equations for the system.";
AMFSystemBoundaryCondition::usage = "AMFSystemBoundaryCondition[sysid] reduces the boundary integrals for the system.";
AMFSystemDirection::usage = "AMFSystemDirection[sysid] automatically defines the direction of analytic continuation for the system.";
AMFSystemSetup::usage = "AMFSystemSetup[sysid, preferred, etac] sets up a single system.";
AMFSystemEndingQ::usage = "AMFSystemEndingQ[preferred, endingscheme] judges whether the system should be an ending system according to the ending scheme, where endingscheme could be a list of defined schems.";
AMFSystemSetupMaster::usage = "AMFSystemSetupMaster[sysid, preferred, endingscheme] sets up a single syetem according to the ending scheme, where endingscheme could be a list of defined schems.";


AMFSystemsSetup::usage = "AMFSystemsSetup[preferred] sets up systems iteratively.";
AMFSystemsTree::usage = "AMFSystemsTree[headsysid] generates the tree structure of the systems.";
AMFSystemsEnding::usage = "AMFSystemsEnding[headsysid] gives the ending systems of a system tree.";


AMFSystemSolution::usage = "AMFSystemSolution[sysid, epslist] solves the input system and its subsystems and stores the results of locally preferred integrals.";
AMFSystemCombineSolution::usage = "AMFSystemCombineSolution[sysids] combines locally preferred integrals results to obtain globally preferred integrals.";
AMFSystemsSolution::usage = "AMFSystemsSolution[sysids, epslist] solves systems iteratively and return the resutls of globally preferred integrals.";


CacheDirectory::usage = "CacheDirectory[key] returns the cache directory determined by key.";
BlackBoxReduce::usage = "BlackBoxReduce[jints, jpreferred, dir] performs black-box reduction from jints to jpreferred.";
BlackBoxDiffeq::usage = "BlackBoxDiffeq[jpreferred, vars, dir] constructs differential equations with jpreferred as preferred masters.";
BlackBoxAMFlowSingle::usage = "BlackBoxAMFlowSingle[jints, epslist, dir] computes jints using black-box auxiliary mass flow, where jints should live in a single physical top-sector.";
BlackBoxAMFlow::usage = "BlackBoxAMFlow[jints, epslist, dir] computes jints using black-box auxiliary mass flow.";


GenerateNumericalConfig::usage = "GenerateNumericalConfig[goal, order] generates a triplet {epslist, workingpre, xorder} for automatic evaluations.";
FitEps::usage = "FitEps[epslist, vlist, leading] fits a Laurant expansion with given interpolations.";
SolveIntegrals::usage = "SolveIntegrals[jints, goal, order] solves given integrals with given precision goal up to given eps order.";


ExpandGaugeX::usage = "ExpandGaugeX[dir] computes the asymptotic expansion of integrals near gaugex=0, where dir is the working directory.";
GenerateSquare::usage = "GenerateSquare[prop, symbol] transforms a linear propagator like l.p+m to l^2+(l.p+m)/symbol.";
SolveIntegralsGaugeLink::usage = "SolveIntegralsGaugeLink[jints, goal, order] solves integrals with gauge link.";


Begin["`Private`"];


(* ::Subsection:: *)
(*common*)


(* ::Subsubsection:: *)
(*package*)


$PackageInfo = {"1.1", "5-Jun-2022"};
$Eps = Symbol["Global`eps"];
$Eta = Symbol["Global`eta"];
$JHead = Symbol["Global`j"];
$CTX = $Context;
$Current = DirectoryName[$InputFileName];
$DESolverPath = FileNameJoin[{$Current, "diffeq_solver", "DESolver.m"}];
$WolframPath = First[$CommandLine];
$AMFlowInfoKeywords = {"Family", "Loop", "Leg", "Conservation", "Replacement", "Propagator", "Numeric", "NThread", "Prescription", "Cut"};


Family := AMFlowInfo["Family"];
Loop := AMFlowInfo["Loop"];
Leg := AMFlowInfo["Leg"];
Conservation := AMFlowInfo["Conservation"];
Replacement := AMFlowInfo["Replacement"];
Propagator := AMFlowInfo["Propagator"];
Numeric := AMFlowInfo["Numeric"];
NThread := AMFlowInfo["NThread"];
Prescription := AMFlowInfo["Prescription"];
Cut := AMFlowInfo["Cut"];


ReducedLeg := Select[Leg, !MemberQ[Keys[Conservation], #]&];
ReducedReplacement := Module[{complete,bare,full},
complete = DeleteDuplicates[Flatten[Outer[Times, ReducedLeg, ReducedLeg]]];
bare = Coefficient[#/.Conservation, complete]&/@Keys[Replacement];
full = GeneralizedTranspose[Append[GeneralizedTranspose[bare], Values[Replacement]]];
If[GeneralizedMatrixRank[bare]<Length[complete], Print["ReducedReplacement: insufficient replacement rules for all independent external scalar products."]; Abort[]];
If[GeneralizedMatrixRank[full]>Length[complete], Print["ReducedReplacement: inconsistent replacement rules."]; Abort[]];
Thread[complete -> GeneralizedRowReduce[full][[;;Length[complete], -1]]]
];
ReducedPropagator := Propagator/.Conservation;


DefinedKeywords[]:=Select[$AMFlowInfoKeywords, Head[AMFlowInfo[#]]=!=AMFlowInfo&];
ExtractGlobalVariables[]:=# -> AMFlowInfo[#]&/@DefinedKeywords[];
DefineGlobalVariables[config_]:=(UnsetGlobalVariables[]; (AMFlowInfo[#[[1]]] = #[[2]])&/@config;);
UnsetGlobalVariables[keys_]:=(Unset[AMFlowInfo[#]]&/@Intersection[keys, DefinedKeywords[]];);
UnsetGlobalVariables[]:=UnsetGlobalVariables[$AMFlowInfoKeywords];
SaveGlobalVariables[n_]:=($Global[n] = ExtractGlobalVariables[];);
LoadGlobalVariables[n_]:=(DefineGlobalVariables[$Global[n]];);
PrintGlobalVariables[]:=(Print/@ExtractGlobalVariables[];);


InitializePackage[]:=Block[{},
Print[MinusString[50]];
Print[StringTemplate["AMFlow: package loaded
Author: Xiao Liu and Yan-Qing Ma
Current version: `ver`
Release date: `date`"][<|"ver" -> $PackageInfo[[1]], "date" -> $PackageInfo[[2]]|>]];
Print[MinusString[50]];
SetAMFOptions@@Options[SetAMFOptions];
SetReductionOptions@@Options[SetReductionOptions];
Print[MinusString[50]];
Print[
"To start evaluating a family of loop integrals, you need to define:
 - AMFlowInfo[\"Family\"] as the name of the family (should be a symbol)
 - AMFlowInfo[\"Loop\"] as a list of loop momenta like {l1, l2}
 - AMFlowInfo[\"Leg\"] as a list of external legs like {p1, p2, p3}
 - AMFlowInfo[\"Conservation\"] as a replacement rule of momentum conservation like {p3 -> -p1-p2}
 - AMFlowInfo[\"Replacement\"] as a list of complete replacement rules for scalar products among external legs like {p1^2 -> 0, p2^2 -> 0, p3^2 -> s} 
 - AMFlowInfo[\"Propagator\"] as a list of complete inverse propagators like {l1^2-msq, l2^2, (l1+l2+p1)^2, ...}
 - AMFlowInfo[\"Numeric\"] as a list of replacement rules for involved mass scales like {s -> 100, msq -> 1}
 - AMFlowInfo[\"NThread\"] as the number of threads in use
For phase-space integrations, you also need to define:
 - AMFlowInfo[\"Prescription\"] as a list with the same length as AMFlowInfo[\"Loop\"], each element being 1, -1 or 0 specifying the Feynman prescription of the corresponding loop, where 1 means +i0, -1 means -i0 and 0 means no prescription at all
 - AMFlowInfo[\"Cut\"] as a list with the same length as AMFlowInfo[\"Propagator\"], each element being 1 or 0 specifying whether the corresponding propagator is on cut or not"];
Print[MinusString[50]];
Print[
"Please keep clear of these global variables during computations:
 - eps, which serves as the dimensional regulator
 - eta, which serves as the auxiliary mass parameter
 - j, which serves as the Head of integrals"];
Print[MinusString[50]];
];


Options[SetAMFOptions] = {"AMFMode" -> {"Prescription", "Mass", "Propagator"}, "EndingScheme" -> {"Tradition", "Cutkosky", "SingleMass"}, "D0" -> 4, "WorkingPre" -> 100, "ChopPre" -> 20, "XOrder" -> 100, "ExtraXOrder" -> 20, "LearnXOrder" -> -1, "TestXOrder" -> 5};
SetAMFOptions[opt___]:=Block[{},
If[MemberQ[Keys[{opt}], "AMFMode"], $AMFMode = "AMFMode"/.{opt}];
If[MemberQ[Keys[{opt}], "EndingScheme"], $EndingScheme = "EndingScheme"/.{opt}];
If[MemberQ[Keys[{opt}], "D0"], $D0 = "D0"/.{opt}];
If[MemberQ[Keys[{opt}], "WorkingPre"], $WorkingPre = "WorkingPre"/.{opt}];
If[MemberQ[Keys[{opt}], "ChopPre"], $ChopPre = "ChopPre"/.{opt}];
If[MemberQ[Keys[{opt}], "XOrder"], $XOrder = "XOrder"/.{opt}];
If[MemberQ[Keys[{opt}], "ExtraXOrder"], $ExtraXOrder = "ExtraXOrder"/.{opt}];
If[MemberQ[Keys[{opt}], "LearnXOrder"], $LearnXOrder = "LearnXOrder"/.{opt}];
If[MemberQ[Keys[{opt}], "TestXOrder"], $TestXOrder = "TestXOrder"/.{opt}];

PrintOptions[SetAMFOptions, $CTX];
If[Head[$AMFMode]=!=List, Print["SetAMFOptions: the option value of \"AMFMode\" should be a list."]; Abort[]];
If[Head[$EndingScheme]=!=List, Print["SetAMFOptions: the option value of \"EndingScheme\" should be a list."]; Abort[]];
];


Options[SetReductionOptions] = {"IBPReducer" -> "FiniteFlow+LiteRed", "BlackBoxRank" -> 3, "BlackBoxDot" -> 0, "ComplexMode" -> True, "DeleteBlackBoxDirectory" -> False};
SetReductionOptions[opt___]:=Block[{},
If[MemberQ[Keys[{opt}], "IBPReducer"], $IBPReducer = "IBPReducer"/.{opt}];
If[MemberQ[Keys[{opt}], "BlackBoxRank"], $BlackBoxRank = "BlackBoxRank"/.{opt}];
If[MemberQ[Keys[{opt}], "BlackBoxDot"], $BlackBoxDot = "BlackBoxDot"/.{opt}];
If[MemberQ[Keys[{opt}], "ComplexMode"], $ComplexMode = "ComplexMode"/.{opt}];
If[MemberQ[Keys[{opt}], "DeleteBlackBoxDirectory"], $DeleteBlackBoxDirectory = "DeleteBlackBoxDirectory"/.{opt}];

PrintOptions[SetReductionOptions, $CTX];
If[MemberQ[Keys[{opt}], "IBPReducer"], GetFile[FileNameJoin[{$Current, "ibp_interface", $IBPReducer, "interface.m"}]]];
];


PrintOptions[func_, ctx_]:=Print[func -> Thread[Keys[Options[func]] -> ToExpression[ctx<>"$"<>#&/@Keys[Options[func]]]]];


(* ::Subsubsection:: *)
(*general functions*)


(*2023-05-13: add "Sequence" to match multi-parameters*)
Options[RunCommand] = {"log" -> False, Sequence@@Options[RunProcess]};
RunCommand[command_, OptionsPattern[]]/;MemberQ[{String,List},Head@command] := Module[
{opt,time,prop,fout,ferr,log,fp},
opt = #[[1]]->OptionValue@#[[1]]&/@Options[RunProcess];
log = OptionValue@"log";
{time, prop} = AbsoluteTiming[RunProcess[{$SystemShell, "-c", command},Sequence@@opt]];
If[log=!=False,
fp = OpenWrite[log <> ".out"];
WriteString[fp, prop["StandardOutput"]];
Close[fp];
fp = OpenWrite[log <> ".err"];
WriteString[fp, prop["StandardError"]];
Close[fp]];
If[prop["ExitCode"]=!=0, Print["RunCommand: process failed" -> command]; Print[prop["StandardError"]]; Abort[]];
time
];


GetFile[file_]:=If[!FileExistsQ[file], Print["GetFile: file not found" -> file]; Abort[], Get[file]];
PutFile[exp_, file_]:=Block[{}, CreateDir[DirectoryName[file]]; Put[exp, file]];
CreateDir[dir_]:=If[!DirectoryQ[dir], CreateDirectory[dir]];
DeleteDir[dir_]:=If[DirectoryQ[dir], DeleteDirectory[dir, DeleteContents -> True]];


GeneralizedTranspose[matrix_]:=If[matrix==={}, {}, Transpose[matrix]];
GeneralizedMatrixRank[matrix_]:=If[matrix==={}, 0, MatrixRank[matrix]];
GeneralizedRowReduce[matrix_]:=If[matrix==={}, {}, RowReduce[matrix]];


ToStringInput[exp_]:=ToString[exp, InputForm];
StringToTemplate[list_]:=StringTemplate[StringJoin[Riffle[list, "\n\n\n"]]];


SameSetQ[set1_,set2_]:=SubsetQ[set1, set2] && SubsetQ[set2, set1];


DynamicPartition[l_,p_]:=MapThread[l[[#;;#2]]&, {{0}~Join~Most@#+1, #}&@Accumulate@p];


MaximalGroup[matrix_]:= Flatten[FirstPosition[#, 1]&/@Select[GeneralizedRowReduce[GeneralizedTranspose[matrix]], !AllTrue[#, #===0&]&]];


MinusString[n_]:=StringJoin@@ConstantArray["-", n];


PutFirst[list_]:=If[list==={}, {}, {list}];
GetFirst[list_]:=If[list==={}, {}, First[list]];


(* ::Subsubsection::Closed:: *)
(*notations*)


ToJ[ind_]:=$JHead[Family, Sequence@@ind];
FromJ[jint_]:=List@@jint[[2;;]];
JSector[jint_]:=ToJ[If[#<=0, 0, 1]&/@FromJ[jint]];
JProp[jint_]:=Length@Select[FromJ[jint], #>0&];
JDot[jint_]:=Total@Select[FromJ[jint]-1, #>0&];
JRank[jint_]:=-Total@Select[FromJ[jint], #<0&];
IntegralWeight[jint_]:={-JProp[jint], -JDot[jint], -JRank[jint], Min[FromJ[jint]], jint};
SortIntegrals[jints_]:=SortBy[jints, IntegralWeight];


GetTopSector[jints_]:=If[Max[#]>0, _, 0]&/@GeneralizedTranspose[FromJ/@jints];
GetTopPosition[jints_]:=Flatten@Position[GetTopSector[jints], _?(#===_&)];


IntegralQ[exp_]:=Head[exp]===$JHead && First[exp]===Family && Length[FromJ[exp]]===Length[SPList[]];
CheckIntegrals[jints_]:=If[#=!={}, Print["CheckIntegrals: something wrong with these integrals" -> #]; Abort[]]&@Pick[jints, IntegralQ/@jints, False];


GetSector[jint_]:=If[#>0, 1, 0]&/@FromJ[jint];
SubSectorQ[sec1_, sec2_]:=AllTrue[sec1-sec2, #<=0&];
TopSectorQ[sec_, listofsec_]:=Count[SubSectorQ[sec, #]&/@listofsec, True]===1;
GetTopSectorList[jlist_]:=Block[{sectors},
sectors = DeleteDuplicates[GetSector/@jlist];
Select[sectors, TopSectorQ[#, sectors]&]
];
SplitTarget[jlist_]:=Block[{topsectors,residue,split,tmp},
topsectors = GetTopSectorList[jlist];
residue = jlist;
split = Table[
tmp = Select[residue, SubSectorQ[GetSector[#], topsectors[[i]]]&];
residue = Complement[residue, tmp];
tmp, {i, Length[topsectors]}];
If[residue=!={}, Print["SplitTarget: residue not empty" -> residue]; Abort[]];
split
];


(* ::Subsubsection::Closed:: *)
(*propagators*)


SPList[]:=Join[DeleteDuplicates[Join@@Outer[Times, Loop, Loop]], Join@@Outer[Times, Loop, ReducedLeg]];
DListSymbol[]:=Denominators/@Range[Length[SPList[]]];


CheckCompleteness[denominators_]:=Module[{rank},
If[Length[denominators]>Length[SPList[]], Print["CheckCompleteness: too many denominators" -> denominators]; Abort[]];
rank = GeneralizedMatrixRank[Coefficient[#/.Conservation, SPList[]]&/@denominators];
If[rank < Length[SPList[]],
Print["CheckCompleteness: denominators are not complete" -> denominators];
Print["CheckCompleteness: try using ToCompleteExplicit[denominators] to make them complete."];
Abort[]];
];


(*denominators should be complete*)
SPListToDListSymbol[denominators_]:=Module[{vars,x,de,sol},
vars = x/@Range[Length[SPList[]]];
de = Expand[denominators/.Conservation]/.Thread[SPList[] -> vars];
sol = Solve[Thread[de==DListSymbol[]], vars][[1]];
Thread[SPList[] -> (vars/.sol)]
];


ToCompleteExplicit[denominators_]:=Module[{pde,all,index},
pde = DeleteDuplicates[Flatten[Outer[Plus, Loop, #]&/@{Loop, ReducedLeg}]/.(2*a_ :> a)]^2;
all = Join[denominators/.Conservation, pde];
index = MaximalGroup[Coefficient[#, SPList[]]&/@all];
all[[index]]
];


ToSquare[denominator_]:=Module[{loop,coe,mom,mass},
loop = Pick[Loop, Exponent[denominator, Loop], 2];
If[loop==={}, Return[$Failed]];
loop = loop[[1]];
coe = Coefficient[denominator/.Conservation, loop, {2, 1, 0}];
{mom, mass} = Expand[Expand[{Sqrt[coe[[1]]]loop+coe[[2]]/(2*Sqrt[coe[[1]]]), coe[[3]]-coe[[2]]^2/(4*coe[[1]])}]/.ReducedReplacement];
If[Intersection[Variables[mass], Loop]=!={}, Return[$Failed]];
{mom, mass}
];
ToSquareAll[denominators_]:=If[!MemberQ[#, $Failed], GeneralizedTranspose[#], Print["ToSquareAll: cannot make squared denominators" -> denominators]; Abort[]]&@(ToSquare/@denominators);
SquaredDenominators[denominators_]:=#[[1]]^2+#[[2]]&@ToSquareAll[denominators];


(* ::Subsubsection::Closed:: *)
(*analyze*)


FeynmanPara[n_]:=FeynmanParameters/@Range[n];


EvaluateABC[denominators_]:=Module[{n,l,e,de,A,B,C,x,Ax,Bx,Cx},
n = Length[denominators];
l = Length[Loop];
e = Length[ReducedLeg];
Table[
de = denominators[[i]]/.Conservation;
A[i] = Table[Which[j===k, Coefficient[de, Loop[[j]]^2], True, 1/2*Coefficient[de, Loop[[j]]*Loop[[k]]]], {j, l}, {k, l}];
B[i] = Table[-1/2*Coefficient[de, Loop[[j]]*ReducedLeg[[k]]], {j, l}, {k, e}] . ReducedLeg;
C[i] = de/.Thread[Loop -> 0], {i, n}];
Total[FeynmanPara[n]*Array[#, n]]&/@{A, B, C}
];


EvaluateUF[denominators_]:=Module[{Ax,Bx,Cx,u,f,f0},
{Ax, Bx, Cx} = EvaluateABC[denominators];
u = If[Ax===0, 0, Expand[Det[Ax]]];
If[u===0, Return[{0, 0, 0}]];
f0 = -ToSquareAll[denominators][[2]] . FeynmanPara[Length[denominators]];
f = Expand[Together[Expand[u(-Cx+Bx . Inverse[Ax] . Bx)]/.ReducedReplacement]-u*f0];
{u, f, f0}
];


AnalyzeComponent[u0_, f_, massterm_]:=Module[{var,loopnum,mass,fterms,vactest,vacQ},
var = Sort[Variables[u0]];
loopnum = Length[Intersection[Variables[If[Head[u0]===Plus, u0[[1]], u0]], var]];
mass = Coefficient[massterm, var];
fterms = If[Head[#]===Plus, List@@#, {#}]&@Expand[f];
vactest = Table[If[Head@fterms[[i]]=!=Times, True, Length@Intersection[List@@fterms[[i]], var] < loopnum+1], {i, Length@fterms}];
vacQ = And@@vactest;
{u0, vacQ, loopnum, var, mass}
];


AnalyzeTopology[denominators_]:=Module[{u,f,massterm,components,info},
{u, f, massterm} = EvaluateUF[denominators];
components = If[Head[#]===Times, List@@#, {#}]&@Factor[u];
info = AnalyzeComponent[#, f, massterm]&/@components;
info = Select[info, #[[3]]>0&];
info = Sort[info, #1[[3]]>=#2[[3]]&];
info
];


(*patts is in fact the indices for master integrals in the family specified by denominators, should only be non-negative integers*)
FactorizeFamily[denominators_, patts_]:=Module[{info,index,u0,vacQ,loopnum,var,mass,mono,tobeloop,mat,x,solved,redef},
info = AnalyzeTopology[denominators];
index = PositionIndex[FeynmanPara[Length[denominators]]];
Table[
{u0, vacQ, loopnum, var, mass} = info[[i]];
mono = If[Head[u0]===Plus, List@@u0, {u0}];
If[vacQ===True && Count[mass, 1]===1 && Count[mass, 0]===Length[mass]-1, 
mono = SortBy[mono, !MemberQ[Variables[#], Pick[var, mass, 1][[1]]]&]];
tobeloop = Flatten[Variables[mono[[1]]]/.index];
mat = Coefficient[#, Loop]&/@ToSquareAll[denominators[[tobeloop]]][[1]];
mat = GeneralizedRowReduce[Append[mat[[#]], -x[#]]&/@Range[Length[mat]]];
solved = FirstPosition[#, 1]&/@mat//Flatten;
redef = Table[Loop[[solved[[k]]]] -> Loop[[solved[[k]]]]-mat[[k]] . Append[Loop, 1], {k, Length[solved]}];
redef = Thread[Keys[redef] -> (Values[redef]/.Thread[x/@Range[Length[mat]] -> Keys[redef]])];
{Keys[redef], Expand[denominators[[Flatten[var/.index]]]/.redef], patts[[All, Flatten[var/.index]]]}, {i, Length[info]}]
];


(* ::Subsubsection::Closed:: *)
(*scheme for eta*)


PrescriptionOfLoop[loop_]:=If[Head[Prescription]===List, Pick[Prescription, Loop, loop][[1]], 1];


PrescriptionOf[prop_]:=Module[{pres},
pres = PrescriptionOfLoop/@Intersection[Variables[prop], Loop];
If[AllTrue[pres, #===0&], Return[0]];
pres = Select[pres, #=!=0&];
If[AllTrue[pres, #===1&], Return[1]];
If[AllTrue[pres, #===-1&], Return[-1]];
$Failed
];


AnalyzeTopSector[topposi_]:=Module[{info,cutinfo,preinfo},
info = AnalyzeTopology[ReducedPropagator[[topposi]]];
cutinfo = If[Head[Cut]===List, Cut[[topposi]], Table[0, Length[topposi]]];
preinfo = PrescriptionOf/@ReducedPropagator[[topposi]];
If[MemberQ[preinfo, $Failed], Print["AnalyzeTopSector: these denominators have conflict prescriptions" -> Pick[ReducedPropagator[[topposi]], preinfo, $Failed]]; Abort[]];
info = {Sequence@@#, cutinfo[[First/@#[[4]]]], preinfo[[First/@#[[4]]]]}&/@info;
info/.Thread[FeynmanPara[Length[topposi]] -> FeynmanPara[Length[ReducedPropagator]][[topposi]]]
];


(*info0: {u0, vacQ, loop, var, mass, cut, pres}*)
VacuumQ[info0_]:=info0[[2]] && AllTrue[info0[[6]], #===0&];
SingleMassQ[info0_]:=VacuumQ[info0] && Count[info0[[5]], 1]===1 && Count[info0[[5]], 0]===Length@info0[[5]]-1;
PhaseVolumeQ[info0_]:=AllTrue[info0[[6]], #===1&] && Length[info0[[4]]]===info0[[3]]+1;
EndingQ[info0_]:=SingleMassQ[info0] || PhaseVolumeQ[info0];


AllPossiblePosition[info0_, "Prescription"]:=Module[{u,vacQ,loopnum,var,mass,cut,pres,props1,props2},
{u, vacQ, loopnum, var, mass, cut, pres} = info0;
PutFirst[First/@Intersection[Pick[var, pres, -1], Pick[var, cut, 0]]]
];
AllPossiblePosition[info0_, "Mass"]:=Module[{u,vacQ,loopnum,var,mass,cut,pres,props,getmass,massiveprops,indepQ},
{u, vacQ, loopnum, var, mass, cut, pres} = info0;
props = Flatten[AllPossiblePosition[info0, "Propagator"]];
Table[getmass[First[var[[i]]]] = mass[[i]], {i, Length@var}];
massiveprops = Select[props, getmass[#]=!=0&];
massiveprops = GatherBy[massiveprops, getmass];
indepQ[mprops_]:=!MemberQ[Variables[Values[ReducedReplacement]], getmass[mprops[[1]]]];
Sort[Select[massiveprops, indepQ], Length[#1]<=Length[#2]&]
];
AllPossiblePosition[info0_, "Propagator"]:=Module[{u,vacQ,loopnum,var,mass,cut,pres,props,branchlength},
{u, vacQ, loopnum, var, mass, cut, pres} = info0;
props = GeneralizedTranspose[{Pick[var, cut, 0]}];
branchlength[v_]:=Length[Complement[var, Variables[Coefficient[u, v, 1]]]];
Map[First, Sort[props, branchlength[#1[[1]]]>=branchlength[#2[[1]]]&], {2}]
];
AllPossiblePosition[info0_, "Branch"]:=Module[{u,vacQ,loopnum,var,mass,cut,pres,findbranch,branches},
{u, vacQ, loopnum, var, mass, cut, pres} = info0;
(*two propgators in the same branch cannot appear in a single monomial of u*)
findbranch[v_]:=Sort[Complement[var, Variables[Coefficient[u, v, 1]]]];
branches = DeleteDuplicates[findbranch/@var];
branches = Intersection[#, Pick[var, cut, 0]]&/@branches;
Map[First, Sort[branches, Length@#1<=Length@#2&], {2}]
];
AllPossiblePosition[info0_, "Loop"]:=Module[{u,vacQ,loopnum,var,mass,cut,pres,findloop,subs,loops},
{u, vacQ, loopnum, var, mass, cut, pres} = info0;
(*x1 x2 + x2 x3 + x3 x1 \[Equal] x1 (x2 + x3) + x2 x3 means x2 and x3 are in the same loop*)
findloop[sub_]:=Sort[Select[If[Head[u]===Plus, List@@u, {u}]*sub^-1, PolynomialQ]];
subs = Times@@@Subsets[var, {loopnum-1}];
loops = DeleteDuplicates@Select[findloop/@subs, #=!={}&];
loops = Intersection[#, Pick[var, cut, 0]]&/@loops;
Map[First, Sort[loops, Length@#1<=Length@#2&], {2}]
];
AllPossiblePosition[info0_, "All"]:=PutFirst[Join@@AllPossiblePosition[info0, "Propagator"]];
AllPossiblePosition[info0_, mode_]:=(Print["AllPossiblePosition: you are using an undefined mode to insert eta: "<>ToStringInput[mode]<>"."];
Print["AllPossiblePosition: please use built-in modes or other defined modes by SetAMFOptions[\"AMFMode\" -> {\"mode1\", \"mode2\", ...}]."];
Print["AllPossiblePosition: built-in modes are \"Prescription\", \"Mass\", \"Propagator\", \"Branch\", \"Loop\" and \"All\"."]; Abort[];);


AMFCandidateComponent[info0_, mode_]:=GetFirst@Which[
!VacuumQ[info0], AllPossiblePosition[info0, mode],
!SingleMassQ[info0], AllPossiblePosition[info0, "Branch"],
True, {}];


AMFCandidate[info_, mode_]:=Module[{candidates},
candidates = AMFCandidateComponent[#, mode]&/@info;
If[mode==="Prescription" || mode==="All", candidates = PutFirst[Join@@candidates]];
GetFirst[Select[candidates, #=!={}&]]
];


AMFPosition[topposi_, mode_]:=If[topposi==={}, {}, AMFCandidate[AnalyzeTopSector[topposi], mode]];
AMFPosition[topposi_, modelist_List]:=GetFirst[Select[AMFPosition[topposi, #]&/@modelist, #=!={}&]];


AMFEtaC[posi_]:=ConstantArray[0, Length[ReducedPropagator]]-Total[UnitVector[Length[ReducedPropagator], #]&/@posi];


(* ::Subsubsection::Closed:: *)
(*boundary*)


BranchToLoop[candidate_]:=If[Det@#===0, $Failed, Thread[Loop -> Inverse[#] . Loop]]&@Normal[CoefficientArrays[candidate, Loop]][[2]];


BranchScale[branch_, scale_]:=Table[If[And@@(FreeQ[branch[[i]], #]&/@Pick[Loop, scale, 1]), 0, 1], {i, Length@branch}];


FindAllRegion[topposi_]:=Module[{branch0,branch,cutbranch,cutposi,trans,patt,regions,pres},
branch0 = ToSquareAll[ReducedPropagator][[1]]/.Thread[ReducedLeg -> 0];
branch = DeleteDuplicates[branch0[[topposi]]];
cutbranch = If[Head[Cut]===List, Pick[branch0, Cut, 1], {}];
cutposi = Flatten@Table[Position[Expand[branch-cutbranch[[i]]], 0], {i, Length@cutbranch}];
trans = Select[BranchToLoop/@Tuples[branch, Length@Loop], #=!=$Failed&];
patt = Tuples[{0, 1}, Length@Loop];
regions = Join@@Table[{trans[[i]], patt[[j]], BranchScale[branch/.trans[[i]], patt[[j]]]}, {i, Length@trans}, {j, Length@patt}];
regions = Select[regions, AllTrue[#[[-1, cutposi]], #===0&]&];
regions = GatherBy[regions, Last][[All, All, ;;2]];
pres = PrescriptionOf/@branch;
regions = Select[#, PrescriptionOf/@Expand[branch/.#[[1]]]===pres&]&/@regions;
If[AnyTrue[regions, #==={}&], Print["FindAllRegion: some regions are prohibited by prescriptions."]; Abort[]];
regions[[All,1]]
];


RegionRule[patt_]:=Thread[Loop -> Power[$Eta, patt/2]Loop];


ExplicitRetionRule[region_]:=Thread[Keys[region[[1]]] -> (Values[region[[1]]]/.RegionRule[region[[2]]])];


RegionPower[integrals_, region_]:=Module[{trans,scale,fullde,factor,powers,p,behavior,beh},
{trans, scale} = region;
fullde = Expand[ReducedPropagator/.trans/.RegionRule[scale]];
factor = If[FreeQ[#, $Eta], 1, $Eta]&/@fullde;
powers = p/@Range[Length@ReducedPropagator];
behavior = Expand[(2-$Eps)Total[scale]-Total@Pick[powers, factor, $Eta]]/.Numeric;
beh[mi_]:=behavior/.Thread[powers -> FromJ[mi]];
beh/@integrals
];


BoundaryPattern[powers_]:=Module[{group,standard,powers2},
group = Gather[powers[[All,1]], IntegerQ[Expand[#1-#2]]&];
Table[standard[group[[i,j]]] = group[[i,1]], {i, Length[group]}, {j, Length[group[[i]]]}];
powers2 = standard[#[[1]]] -> Expand[# - standard[#[[1]]]]&/@powers;
#[[1,1]] + Max/@GeneralizedTranspose[#[[All,2]]]&/@GatherBy[powers2, Keys]
];


FactorSum[exp_Plus]:=Module[{gcd},
gcd = PolynomialGCD@@exp;
{gcd, Plus@@(Factor[List@@exp*gcd^-1])}
];


Attributes[SimplifyIntegrand] = {Listable};
SimplifyIntegrand[int_]:=Module[{factors,sums,others,factorsum},
factors = If[Head[int]===Times, List@@int, {int}];
sums = Select[factors, Head[#]===Plus&];
others = Times@@Select[factors, Head[#]=!=Plus&];
factorsum = FactorSum/@sums;
Factor[Times@@factorsum[[All,1]]*others]*Times@@factorsum[[All,2]]
];


BoundaryIntegrands[integrals_, border_, region_]:=Module[{trans,scale,fullde,factor,expde,completede,sptoD,structure,rules,x,rulex,rules0,powers,p,integrand,behavior,int,integrands},
{trans, scale} = region;
fullde = Expand[ReducedPropagator/.trans/.RegionRule[scale]];
factor = If[FreeQ[#, $Eta], 1, $Eta]&/@fullde;
fullde = Expand[fullde*factor^-1];
expde = Coefficient[fullde, $Eta, 0];
completede = ToCompleteExplicit[expde[[GetTopPosition[integrals]]]];
sptoD = SPListToDListSymbol[completede];
fullde = Expand[fullde/.sptoD]/.ReducedReplacement;
expde = Expand[expde/.sptoD]/.ReducedReplacement;

structure = Coefficient[#, $Eta, {0, -1/2, -1}]&/@fullde;
rules = Thread[Flatten[Table[x[i, j], {i, Length@expde}, {j, 3}]] -> Flatten[structure]];
rulex = Flatten[If[Length@#===1, {}, Thread[#[[2;;]] -> #[[1]]]]&/@(Keys/@GatherBy[rules, Values])];
rules0 = Select[rules, #[[2]]===0&];

powers = p/@Range[Length@expde];
integrand = Times@@Table[Power[x[i, 1]+x[i, 2]Power[$Eta, -1/2]+x[i, 3]*$Eta^-1, -p[i]], {i, Length@expde}]/.rules0/.rulex;
integrand = Together[SeriesCoefficient[integrand, {$Eta, Infinity, #}]&/@Range[0, Max[border]]];
integrand = integrand/.rules;

int[k_]:=integrand[[;;border[[k]]+1]]/.Thread[powers -> FromJ[integrals[[k]]]];
integrands = int/@Range[Length[integrals]];

{SquaredDenominators[completede], SimplifyIntegrand[integrands]}
];


ApartRationals[rationals_]:=Module[{apart},
apart[rat_List,var_]:=Module[{num,de,apartde},
num = Numerator@rat;
de = Denominator@rat;
apartde = Apart[de^-1, var];
Flatten[num(If[Head@#===Plus, List@@#, {#}]&/@apartde)]
];
Fold[apart, {#}, DListSymbol[]]&/@rationals
];


LaportaIntegrals[term_]:=Module[{ruled},
ruled = CoefficientRules[Denominator[term], DListSymbol[]];
If[Length@ruled=!=1, Print["LaportaIntegrals: denominator is not a monomial of DListSymbol" -> term]; Abort[]];
ruled[[1,1]]-#[[1]] -> #[[2]]*ruled[[1, 2]]^-1&/@CoefficientRules[Numerator[term], DListSymbol[]]
];


BoundaryIntegrals[completede_,integrands_]:=Module[{finddiff,analyze,partition,integrands2,apartdes,family,familyrule,families,integrals},
finddiff[list_,di_]:=Module[{tmp},
tmp = Cases[list, Plus[di, a_]:>a];
If[tmp==={}, 0, tmp[[1]]]
];

analyze[term_]:=Module[{factors},
factors = FactorList[Denominator[term]][[All,1]];
finddiff[factors, #]&/@DListSymbol[]
];

partition = Length/@integrands;
integrands2 = Join@@integrands;
apartdes = ApartRationals[Denominator[integrands2]^-1];
apartdes = Together[Table[Plus@@@GatherBy[apartdes[[i]], analyze], {i, Length@apartdes}]];
integrands2 = Numerator[integrands2]*apartdes;

Table[
family = analyze[integrands2[[i, j]]];
familyrule = Thread[DListSymbol[] -> DListSymbol[]-family];
integrands2[[i, j]] = {family, LaportaIntegrals[integrands2[[i, j]]/.familyrule]}, 
{i, Length@integrands2}, {j, Length@integrands2[[i]]}];

families = DeleteDuplicates[Join@@integrands2[[All, All, 1]]];
integrals[i_, fam_]:={};
Table[integrals[i, integrands2[[i, j, 1]]] = integrands2[[i, j, 2]], {i, Length@integrands2}, {j, Length@integrands2[[i]]}];

Table[{completede+families[[i]], DynamicPartition[integrals[#, families[[i]]]&/@Range[Length@integrands2], partition]}, {i, Length@families}]
];


ReduceBoundary[integrals_, border_, region_, dir_]:=Module[{powers,boundary,prop,allintegrals,thisid,cutde,cut,config,masters,rules,f,table,str,coe},
powers = RegionPower[integrals, region];
boundary = BoundaryIntegrals@@BoundaryIntegrands[integrals, border, region];
Table[
prop = boundary[[i, 1]];
allintegrals = DeleteDuplicates[Keys/@Flatten[boundary[[i,2]],2]];

SaveGlobalVariables[thisid];
If[Head[Cut]===List,
cutde = Pick[ReducedPropagator/.region[[1]], Cut, 1];
cut = If[MemberQ[Expand[#-cutde]/.ReducedReplacement, 0], 1, 0]&/@prop;
If[Count[cut, 1]=!=Count[Cut, 1], Print["ReduceBoundary: eta may have been inserted to cut denominators."]; Abort[]];
AMFlowInfo["Cut"] = cut];
AMFlowInfo["Propagator"] = prop;
config = ExtractGlobalVariables[];
{masters, rules} = BlackBoxReduce[ToJ/@allintegrals, {}, dir];
LoadGlobalVariables[thisid];

If[masters==={}, Nothing[],
ClearAll[f];
f[___]:=ConstantArray[0, Length@masters];
Table[f[FromJ@rules[[k, 1]]] = rules[[k, 2]], {k, Length@rules}];
table = Table[
str = boundary[[i, 2, j, k]];
coe = Together[Total[f[#[[1]]]*#[[2]]&/@str]/.Numeric];
If[coe===0, coe = ConstantArray[0, Length@masters]];
coe, {j, Length@boundary[[i,2]]}, {k,Length@boundary[[i,2,j]]}];
{powers, config, masters, table}], {i, Length@boundary}]
];


(* ::Subsubsection::Closed:: *)
(*single-mass vacuum*)


Vacuum[1, 1] := -Gamma[-1 + $Eps];
Vacuum[2, 3] := -Gamma[1 - $Eps]^2*Gamma[$Eps]*Gamma[-1 + 2*$Eps]/Gamma[2 - $Eps];
Vacuum[3, 4] := Gamma[1 - $Eps]^3*Gamma[-1 + 2*$Eps]*Gamma[-2 + 3*$Eps]/Gamma[2 - $Eps];
Vacuum[3, 5] := -Gamma[2 - 3*$Eps]*Gamma[1 - $Eps]^4*Gamma[$Eps]^2*Gamma[-1 + 3*$Eps]/(Gamma[2 - 2*$Eps]^2*Gamma[2 - $Eps]);
Vacuum[4, 5] := -Gamma[1 - $Eps]^4*Gamma[-2 + 3*$Eps]*Gamma[-3 + 4*$Eps]/Gamma[2 - $Eps];
Vacuum[a___] := (Print["Vacuum: undefined single-mass vacuum integral" -> {a}]; Abort[];);


(* ::Subsection:: *)
(*auxiliary mass flow*)


(* ::Subsubsection::Closed:: *)
(*file structures*)


AMFSystemDirectory[sysid_]:=FileNameJoin[{$AMFDirectory, ToStringInput[sysid]}];
AMFSystemNextID[]:=Block[{i = 1}, While[DirectoryQ[AMFSystemDirectory[i]], i++]; i];
AMFSystemPathTo[sysid_, keyword_]:=FileNameJoin[{AMFSystemDirectory[sysid], AMFSystemPath[keyword]}];


AMFSystemPath["Config"] = "config";
AMFSystemPath["Preferred"] = "preferred";
AMFSystemPath["GlobalPreferred"] = "globalpreferred";
AMFSystemPath["SubSystem"] = "subsystem";
AMFSystemPath["DiffeqSetup"] = "diffeqsetup";
AMFSystemPath["Masters"] = "masters";
AMFSystemPath["Diffeq"] = "diffeq";
AMFSystemPath["BPattern"] = "bpattern";
AMFSystemPath["BOrder"] = "border";
AMFSystemPath["BoundaryReduce"] = "boundaryreduce";
AMFSystemPath["Boundary"] = "boundary";
AMFSystemPath["Direction"] = "direction";
AMFSystemPath["BoundaryMI"] = "boundarymi";
AMFSystemPath["Solution"] = "solution";
AMFSystemPath["EpsList"] = "epslist";
AMFSystemPath[a___]:=(Print["AMFSystemPath: undefined keyword" -> {a}]; Abort[];);


AMFSystemWrite[sysid_, keyword_, exp_]:=PutFile[exp, AMFSystemPathTo[sysid, keyword]];
AMFSystemRead[sysid_, keyword_]:=GetFile[AMFSystemPathTo[sysid, keyword]];


(* ::Subsubsection:: *)
(*setup a system*)


AMFSystemInitialize[sysid_, preferred_, etac_]:=Module[{thisid},
SaveGlobalVariables[thisid];
AMFlowInfo["Propagator"] = ReducedPropagator + $Eta*etac;
PrintGlobalVariables[];
AMFSystemWrite[sysid, "Config", ExtractGlobalVariables[]];
AMFSystemWrite[sysid, "Preferred", preferred];
LoadGlobalVariables[thisid];
];


AMFSystemDifferentialEquation[sysid_]:=Module[{time1,thisid,res,time2},
Print[StringTemplate["AMFSystemDifferentialEquation: preparing differential equations for system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];
SaveGlobalVariables[thisid];
DefineGlobalVariables[AMFSystemRead[sysid, "Config"]];
res = BlackBoxDiffeq[AMFSystemRead[sysid, "Preferred"], {$Eta}, AMFSystemPathTo[sysid, "DiffeqSetup"]];
AMFSystemWrite[sysid, "Masters", res[[1]]];
AMFSystemWrite[sysid, "Diffeq", res[[3, 1]]];
LoadGlobalVariables[thisid];
time2 = AbsoluteTime[];
Print[StringTemplate["AMFSystemDifferentialEquation: differential equations for system `1` prepared in `2`s."][ToStringInput[sysid], ToStringInput[Ceiling[time2-time1]]]];
];


AMFSystemBoundaryOrder[sysid_]:=Module[{time1,template,rule,thisid,demi,regions,powers,pattern,name,time2},
Print[StringTemplate["AMFSystemBoundaryOrder: determining boundary orders for system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];
template = StringToTemplate[{
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`desolver`];
SetDefaultOptions[];
SetExpansionOptions[\"ExtraXOrder\" -> `exorder`];",
"If[!FileExistsQ[`diffeq`], Print[\"error: diffeq not found.\"]; Abort[]];
diffeq = Get[`diffeq`];
{regions, powers, pattern} = Get[`bpattern`];",
"epsrule = {eps \[Rule] 10^-4};
deinf = Together[-Power[eta,-2](diffeq/.epsrule/.eta -> 1/eta)];
npattern = -pattern/.epsrule;
orders = DetermineBoundaryOrder[deinf, #]&/@npattern;",
"border = Table[
k = Select[Range[Length[pattern]], IntegerQ[pattern[[#, 1]]-powers[[i, 1]]]&][[1]];
orders[[k]]-(pattern[[k]]-powers[[i]]),  {i, Length[powers]}];
border = Map[If[#<0,-1,#]&, border, {2}];",
"Put[border, `border`];
Quit[];"}];

rule = <|
"desolver" -> ToStringInput[$DESolverPath], 
"exorder" -> ToStringInput[$ExtraXOrder],
"diffeq" -> ToStringInput[AMFSystemPath["Diffeq"]],
"bpattern" -> ToStringInput[AMFSystemPath["BPattern"]],
"border" -> ToStringInput[AMFSystemPath["BOrder"]]
|>;

SaveGlobalVariables[thisid];
DefineGlobalVariables[AMFSystemRead[sysid, "Config"]];
demi = AMFSystemRead[sysid, "Masters"];
regions = FindAllRegion[GetTopPosition[demi]];
powers = RegionPower[demi, #]&/@regions;
pattern = BoundaryPattern[powers];
AMFSystemWrite[sysid, "BPattern", {regions, powers, pattern}];
LoadGlobalVariables[thisid];

name = AMFSystemPathTo[sysid, "BOrder"];
FileTemplateApply[template, rule, name<>".wl"];
RunCommand[$WolframPath<>" -noprompt -script "<>name<>".wl", "log" -> name<>".log"];
time2 = AbsoluteTime[];
Print[StringTemplate["AMFSystemBoundaryOrder: boundary orders for system `1` determined in `2`s."][ToStringInput[sysid], ToStringInput[Ceiling[time2-time1]]]];
];


AMFSystemBoundaryCondition[sysid_]:=Module[{time1,demi,border,regions,powers,pattern,thisid,boundaries,reduce,time2},
Print[StringTemplate["AMFSystemBoundaryCondition: preparing boundary integrals for system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];
demi = AMFSystemRead[sysid, "Masters"];
border = AMFSystemRead[sysid, "BOrder"];
{regions, powers, pattern} = AMFSystemRead[sysid, "BPattern"];
Print[StringTemplate["AMFSystemBoundaryCondition: `1` possible integration regions near the Infinity."][ToStringInput[Length[regions]]]];

SaveGlobalVariables[thisid];
DefineGlobalVariables[AMFSystemRead[sysid, "Config"]];
boundaries = Join@@Table[
Print[StringTemplate["AMFSystemBoundaryCondition: region `1` with transformation `2`."][ToStringInput[i], ToStringInput[ExplicitRetionRule[regions[[i]]]]]];
reduce = ReduceBoundary[demi, border[[i]], regions[[i]], AMFSystemPathTo[sysid, "BoundaryReduce"]];
Print[StringTemplate["AMFSystemBoundaryCondition: region `1` finished."][ToStringInput[i]]];
reduce, {i, Length[regions]}];
AMFSystemWrite[sysid, "Boundary", boundaries];
LoadGlobalVariables[thisid];
time2 = AbsoluteTime[];
Print[StringTemplate["AMFSystemBoundaryCondition: boundary integrals for system `1` prepared in `2`s."][ToStringInput[sysid], ToStringInput[Ceiling[time2-time1]]]];
];


AMFSystemDirection[sysid_]:=Module[{thisid,prop,pres,direction},
SaveGlobalVariables[thisid];
DefineGlobalVariables[AMFSystemRead[sysid, "Config"]];
prop = Select[ReducedPropagator, !FreeQ[#, $Eta]&];
pres = PrescriptionOf[Total[Variables[prop]]];
If[pres===$Failed, Print["AMFSystemDirection: cannot define a self-consistent direction for continuation."]; Abort[]];
direction = If[pres===-1, "Im", "NegIm"];
AMFSystemWrite[sysid, "Direction", direction];
LoadGlobalVariables[thisid];
Print[StringTemplate["AMFSystemDirection: direction of continuation for system `1` is `2`."][ToStringInput[sysid], ToStringInput[direction]]];
];


AMFSystemSetup[sysid_, preferred_, etac_]:=Module[{time1,thisid,time2},
Print[StringTemplate["AMFSystemSetup: setting up system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];
Print[MinusString[15]];
AMFSystemInitialize[sysid, preferred, etac];
Print[MinusString[15]];

If[AllTrue[etac, #===0&], Print["AMFSystemSetup: this is an ending system."],
AMFSystemDifferentialEquation[sysid];
Print[MinusString[15]];
AMFSystemBoundaryOrder[sysid];
Print[MinusString[15]];
AMFSystemBoundaryCondition[sysid];
Print[MinusString[15]];
AMFSystemDirection[sysid]];

Print[MinusString[15]];
time2 = AbsoluteTime[];
Print[StringTemplate["AMFSystemSetup: system `1` set up in `2`s."][ToStringInput[sysid], ToStringInput[Ceiling[time2-time1]]]];
];


AMFSystemEndingQ[preferred_, schemes_List]:=AllTrue[AMFSystemEndingQ[preferred, #]&/@schemes, TrueQ];
AMFSystemEndingQ[preferred_, "Tradition"]:=AMFPosition[GetTopPosition[preferred], $AMFMode]==={};
AMFSystemEndingQ[preferred_, "Cutkosky"]:=Module[{posi,info,cutcom},
posi = GetTopPosition[preferred];
info = AnalyzeTopSector[posi];
cutcom = Select[info, PhaseVolumeQ];
!(AllTrue[EndingQ/@info, TrueQ] && Length[cutcom]===1)
];
AMFSystemEndingQ[preferred_, "SingleMass"]:=Module[{posi,info,cutcom},
posi = GetTopPosition[preferred];
info = AnalyzeTopSector[posi];
cutcom = Select[info, PhaseVolumeQ];
!(AllTrue[EndingQ/@info, TrueQ] && Length[cutcom]===0 && Length[Loop]>0 && AllTrue[Join@@(FromJ/@preferred), #>=0&])
];


AMFSystemSetupMaster[sysid_, preferred_, schemes_List]:=Module[{endingQ,scheme},
endingQ = AMFSystemEndingQ[preferred, #]&/@schemes;
If[AllTrue[endingQ, TrueQ],
AMFSystemSetup[sysid, preferred, Table[0, Length[ReducedPropagator]]];
AMFSystemWrite[sysid, "GlobalPreferred", {preferred, Table[1, Length[preferred]], preferred, Identity}];
{sysid},
scheme = Pick[schemes, endingQ, False][[1]];
AMFSystemSetupMaster[sysid, preferred, scheme]]
];
AMFSystemSetupMaster[sysid_, preferred_, "Tradition"]:=Module[{posi,etac},
posi = GetTopPosition[preferred];
etac = AMFEtaC@AMFPosition[posi, $AMFMode];
AMFSystemSetup[sysid, preferred, etac];
AMFSystemWrite[sysid, "GlobalPreferred", {preferred, Table[1, Length[preferred]], preferred, Identity}];
{sysid}
];
AMFSystemSetupMaster[sysid_, preferred_, "Cutkosky"]:=Module[{posi,info,cutcom,thisid,prefactor,etac},
posi = GetTopPosition[preferred];
info = AnalyzeTopSector[posi];
cutcom = Select[info, PhaseVolumeQ];
SaveGlobalVariables[thisid];
UnsetGlobalVariables[{"Prescription", "Cut"}];
prefactor = ConstantArray[2 (Pi^(2-$Eps)*(2Pi)^(2$Eps-4))^cutcom[[1,3]] (-1)^(cutcom[[1,3]]+1), Length[preferred]];
etac = AMFEtaC@AMFPosition[posi, $AMFMode];
AMFSystemSetup[sysid, preferred, etac];
AMFSystemWrite[sysid, "GlobalPreferred", {preferred, prefactor, preferred, Im}];
LoadGlobalVariables[thisid];
{sysid}
];
AMFSystemSetupMaster[sysid_, preferred_, "SingleMass"]:=Module[{posi,faminfo,thisid,loop,prop,mas,drop,droploop,jpre,prefactor,m,n,etac},
posi = GetTopPosition[preferred];
faminfo = FactorizeFamily[ReducedPropagator[[posi]]/.Thread[ReducedLeg -> 0], FromJ[#][[posi]]&/@preferred];
Table[
SaveGlobalVariables[thisid];
{loop, prop, mas} = faminfo[[i]];
drop = Position[ToSquareAll[prop][[2]], -1][[1, 1]];
droploop = ToSquare[prop[[drop]]][[1]];
AMFlowInfo["Loop"] = Select[loop, #=!=droploop&];
AMFlowInfo["Leg"] = {droploop};
AMFlowInfo["Conservation"] = {};
AMFlowInfo["Replacement"] = {droploop^2 -> -1};
AMFlowInfo["Propagator"] = ToCompleteExplicit[Drop[prop, {drop}]];
AMFlowInfo["Numeric"] = {};
UnsetGlobalVariables[{"Prescription", "Cut"}];
jpre = ToJ[PadRight[Drop[#, {drop}], Length[ReducedPropagator]]]&/@mas;
prefactor = Simplify[((-1)^(-n)*Gamma[2-$Eps-m]*Gamma[-2+$Eps+m+n])*(Gamma[2-$Eps]*Gamma[n])^-1/.{n -> mas[[All, drop]], m -> Total/@mas-mas[[All, drop]]-(Length[loop]-1)*(2-$Eps)}];
etac = AMFEtaC@AMFPosition[GetTopPosition[jpre], $AMFMode];
AMFSystemSetup[sysid+i-1, DeleteDuplicates[jpre], etac];
AMFSystemWrite[sysid+i-1, "GlobalPreferred", {preferred, prefactor, jpre, Identity}];
LoadGlobalVariables[thisid];
sysid+i-1, {i, Length[faminfo]}]
];


(* ::Subsubsection::Closed:: *)
(*iterative setup*)


AMFSystemsSetup[preferred_]:=Module[{sysids,thisid,boundary,subsystem},
If[preferred==={}, Print["AMFSystemsSetup: no preferred masters input."]; Abort[]];
Print[MinusString[30]];
sysids = AMFSystemSetupMaster[AMFSystemNextID[], preferred, $EndingScheme];

SaveGlobalVariables[thisid];
Table[subsystem = {};
If[FileExistsQ[AMFSystemPathTo[sysid, "Masters"]],
boundary = AMFSystemRead[sysid, "Boundary"];
Table[
DefineGlobalVariables[boundary[[i, 2]]];
AppendTo[subsystem, AMFSystemsSetup[boundary[[i, 3]]]], {i, Length@boundary}]];
AMFSystemWrite[sysid, "SubSystem", subsystem], {sysid, sysids}];
LoadGlobalVariables[thisid];

sysids
];


AMFSystemsTree[headsysid_]:=Module[{rule},
rule = {headsysid -> AMFSystemRead[headsysid, "SubSystem"]};
Join[rule, Sequence@@AMFSystemsTree/@Flatten[Values[rule]]]
];


AMFSystemsEnding[headsysid_]:=Keys@Select[AMFSystemsTree[headsysid], #[[2]]==={}&];


(* ::Subsubsection:: *)
(*solve*)


AMFSystemSolution[sysid_, epslist_]:=Module[{time1,preferred,subsystem,boundarymi,template,rule,name,time2},
Print[StringTemplate["AMFSystemSolution: solving system `1`."][ToStringInput[sysid]]];
time1 = AbsoluteTime[];

preferred = AMFSystemRead[sysid, "Preferred"];
subsystem = AMFSystemRead[sysid, "SubSystem"];
AMFSystemWrite[sysid, "EpsList", epslist];

If[subsystem==={} && GetTopPosition[preferred]==={}, 
Print[StringTemplate["AMFSystemSolution: boundary integrals of system `1` are trivial."][ToStringInput[sysid]]];
AMFSystemWrite[sysid, "Solution", Thread[preferred -> ConstantArray[1, {Length[preferred], Length[epslist]}]]]];

If[subsystem==={} && GetTopPosition[preferred]=!={},
Print[StringTemplate["AMFSystemSolution: boundary integrals of system `1` cannot be solved with built-in functions."][ToStringInput[sysid]]];
Print[StringTemplate["AMFSystemSolution: you can either input the solutions by hand in advance using AMFSystemWrite[`1`, \"Solution\", yoursolutions], or set \"AMFMode\" and \"EndingScheme\" to their default values by SetAMFOptions before running."][ToStringInput[sysid]]];
Print["AMFSystemSolution: use Options[SetAMFOptions] to check the default option values."]];

If[subsystem=!={},
Print[StringTemplate["AMFSystemSolution: boundary integrals of system `1` will be obtained from solutions of subsystems."][ToStringInput[sysid]]];
Print[StringTemplate["AMFSystemSolution: subsystems of system `1` are `2`."][ToStringInput[sysid], ToStringInput[subsystem]]];
boundarymi = {};
Table[Print[StringTemplate["AMFSystemSolution: now dealing with subsystems `1` of system `2`."][ToStringInput[subsystem[[i]]], ToStringInput[sysid]]];
AppendTo[boundarymi, AMFSystemsSolution[subsystem[[i]], epslist]];
Print[StringTemplate["AMFSystemSolution: subsystems `1` of system `2` done."][ToStringInput[subsystem[[i]]], ToStringInput[sysid]]], {i, Length@subsystem}];
AMFSystemWrite[sysid, "BoundaryMI", boundarymi];

template = StringToTemplate[{
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`desolver`];",
"masters = Get[`masters`];
diffeq = Get[`diffeq`];
boundary = Get[`boundary`];
direction = Get[`direction`];
epslist = Get[`epslist`];
boundaryMI = Get[`boundarymi`];
{regions, powers, pattern} = Get[`bpattern`];",
"pattern = Select[pattern, AnyTrue[Flatten[#[[1]]-boundary[[All,1]]], IntegerQ]&];",
"sol[n_]:=Module[{epsrule, evaluate, tobcs, bmi, de, bc},
SetDefaultOptions[];
SetGlobalOptions[\"WorkingPre\" -> `wkpre`, \"ChopPre\" -> `cppre`];
SetExpansionOptions[\"XOrder\" -> `xorder`, \"ExtraXOrder\" -> `exorder`, \"LearnXOrder\" -> `lorder`, \"TestXOrder\" -> `torder`];
SetRunningOptions[\"RunDirection\" -> direction];
evaluate[tab_,list_]:=Map[#.list&,tab/.epsrule,{2}];
tobcs[beh_,tab_]:=MapThread[Thread[#1-Range[0,Length@#2-1]->#2]& ,{beh/.epsrule,tab}];
epsrule = {eps -> epslist[[n]]};
bmi = Map[N[#, $MinPrecision]&, Values[boundaryMI][[All,All,n]], {2}];
de = Together[diffeq/.epsrule];
bc = MapThread[tobcs[#1, evaluate[#2, #3]]&, {boundary[[All,1]], boundary[[All,-1]], bmi}];
bc = Join@@@Transpose[bc];
bc = MapThread[Join[#1,#2]&, {bc, Transpose[Thread/@Thread[(pattern/.epsrule) -> 0]]}];
AMFlow[de, bc]];",
"LaunchKernels[`kernel`];
results = ParallelTable[sol[i], {i, Length[epslist]}, DistributedContexts -> All];
CloseKernels[];",
"Put[Thread[masters -> Transpose[results]], `solution`];",
"Quit[];"}];

rule = <|
"desolver" -> ToStringInput[$DESolverPath],
"masters" -> ToStringInput[AMFSystemPath["Masters"]],
"diffeq" -> ToStringInput[AMFSystemPath["Diffeq"]],
"boundary" -> ToStringInput[AMFSystemPath["Boundary"]],
"direction" -> ToStringInput[AMFSystemPath["Direction"]],
"epslist" -> ToStringInput[AMFSystemPath["EpsList"]],
"boundarymi" -> ToStringInput[AMFSystemPath["BoundaryMI"]],
"bpattern" -> ToStringInput[AMFSystemPath["BPattern"]],
"wkpre" -> ToStringInput[$WorkingPre],
"cppre" -> ToStringInput[$ChopPre],
"xorder" -> ToStringInput[$XOrder],
"exorder" -> ToStringInput[$ExtraXOrder],
"lorder" -> ToStringInput[$LearnXOrder],
"torder" -> ToStringInput[$TestXOrder],
"kernel" -> ToStringInput[NThread],
"solution" -> ToStringInput[AMFSystemPath["Solution"]]
|>;

name = AMFSystemPathTo[sysid, "Solution"];
FileTemplateApply[template, rule, name<>".wl"];
Print[StringTemplate["AMFSystemSolution: all subsystems of system `1` done, solving differential equations."][ToStringInput[sysid]]];
RunCommand[$WolframPath<>" -noprompt -script "<>name<>".wl", "log"->name<>".log"];
Print[StringTemplate["AMFSystemSolution: differential equations of system `1` solved."][ToStringInput[sysid]]]];

time2 = AbsoluteTime[];
Print[StringTemplate["AMFSystemSolution: system `1` solved in `2`s."][ToStringInput[sysid], ToStringInput[Ceiling[time2-time1]]]];
];


AMFSystemCombineSolution[sysids_]:=Module[{all,global,coe,local,op,sol,epslist},
all = Table[
{global, coe, local, op} = AMFSystemRead[sysid, "GlobalPreferred"];
sol = AMFSystemRead[sysid, "Solution"];
epslist = AMFSystemRead[sysid, "EpsList"];
If[!SubsetQ[Keys[sol], local], Print["AMFSystemCombineSolution: some boundary integrals have not been solved yet in this system" -> sysid]; Abort[]];
Thread[global -> GeneralizedTranspose[coe/.$Eps -> #&/@epslist]*op[local/.sol]], {sysid, sysids}];
If[SameQ@@Keys[all]=!=True, Print["AMFSystemCombineSolution: unmatched systems" -> sysids]; Abort[]];
Thread[Keys[all][[1]] -> Times@@Values[all]]
];


AMFSystemsSolution[sysids_, epslist_]:=Block[{},
Table[Print[MinusString[30]];
AMFSystemSolution[sysid, epslist], {sysid, sysids}];
AMFSystemCombineSolution[sysids]
];


(* ::Subsection:: *)
(*black-box functions*)


(* ::Subsubsection::Closed:: *)
(*basic usage*)


CacheDirectory[key_]:=FileNameJoin[{If[#==="", Directory[], #]&[DirectoryName[If[$FrontEnd===Null, $InputFileName, NotebookFileName[]]]], "cache", ToStringInput[Family]<>"_"<>key}];


BlackBoxReduce[jints_, jpreferred_, dir_:Unevaluated[CacheDirectory["reduce"]]]:=Block[{dir0,top,rank,dot,res},
CheckCompleteness[ReducedPropagator];
CheckIntegrals[jints];
CheckIntegrals[jpreferred];
If[jints==={}, Return[{{}, {}}]];
If[Loop==={}, Return[{{ToJ[{}]}, {ToJ[{}] -> {1}}}]];
dir0 = Evaluate[dir];
top = GetTopSector[Join[jints, jpreferred]];
rank = Max[$BlackBoxRank, JRank/@Join[jints, jpreferred]];
dot = Max[$BlackBoxDot, JDot/@Join[jints, jpreferred]];
IBPSystem[top, rank, dot, jpreferred, $ComplexMode, dir0];
res = AnalyticReduction[jints];
If[$DeleteBlackBoxDirectory===True, DeleteDir[dir0]];
res
];


BlackBoxDiffeq[jpreferred_, vars_, dir_:Unevaluated[CacheDirectory["diffeq"]]]:=Block[{dir0,top,rank,dot,res},
CheckCompleteness[ReducedPropagator];
CheckIntegrals[jpreferred];
If[jpreferred==={}, Print["BlackBoxDiffeq: no preferred masters input."]; Abort[]];
dir0 = Evaluate[dir];
top = GetTopSector[jpreferred];
rank = Max[$BlackBoxRank, JRank/@jpreferred];
dot = Max[$BlackBoxDot, JDot/@jpreferred+1];
IBPSystem[top, rank, dot, jpreferred, $ComplexMode, dir0];
res = DifferentialEquation[vars];
If[$DeleteBlackBoxDirectory===True, DeleteDir[dir0]];
res
];


BlackBoxAMFlowSingle[jints_, epslist_, dir_:Unevaluated[CacheDirectory["amflow"]]]:=Block[{times,$AMFDirectory,time1,masters,rules,time2,sol,sysids,zeroints,timef},
CheckCompleteness[ReducedPropagator];
CheckIntegrals[jints];
Print["BlackBoxAMFlowSingle: started."];
times = AbsoluteTime[];
$AMFDirectory = Evaluate[dir];
Print[StringTemplate["BlackBoxAMFlowSingle: working directory is `1`."][ToStringInput[$AMFDirectory]]];

Print["BlackBoxAMFlowSingle: reducing target integrals."];
time1 = AbsoluteTime[];
{masters, rules} = BlackBoxReduce[jints, {}, AMFSystemDirectory[0]];
time2 = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlowSingle: target integrals reduced in `1`s."][ToStringInput[Ceiling[time2-time1]]]];

If[masters==={}, Print["BlackBoxAMFlowSingle: trivial target integrals."];  sol = {},

Print["BlackBoxAMFlowSingle: constructing amflow systems."];
time1 = AbsoluteTime[];
sysids = AMFSystemsSetup[masters];
time2 = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlowSingle: amflow systems constructed in `1`s."][ToStringInput[Ceiling[time2-time1]]]];

Print["BlackBoxAMFlowSingle: solving amflow systems."];
time1 = AbsoluteTime[];
sol = AMFSystemsSolution[sysids, epslist];
time2 = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlowSingle: amflow systems solved in `1`s."][ToStringInput[Ceiling[time2-time1]]]];

If[!SubsetQ[Keys[sol], masters], Print["BlackBoxAMFlowSingle: some master integrals have not been solved yet."]; Abort[]];
sol = GeneralizedTranspose[masters/.sol];
sol = GeneralizedTranspose@Table[(Values[rules]/.$Eps -> epslist[[i]]) . sol[[i]], {i, Length[epslist]}];
sol = Thread[Keys[rules] -> sol]];

zeroints = Complement[jints, Keys[rules]];
sol = Join[sol, Thread[zeroints -> ConstantArray[0, {Length[zeroints], Length[epslist]}]]];
If[$DeleteBlackBoxDirectory===True, DeleteDir[$AMFDirectory]];
timef = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlowSingle: finished in `1`s."][ToStringInput[Ceiling[timef-times]]]];

sol
];


BlackBoxAMFlow[jints_, epslist_, dir_:Unevaluated[CacheDirectory["amflow"]]]:=Block[{targets,sol,times,timef},
Print["BlackBoxAMFlow: started."];
times = AbsoluteTime[];
targets = SplitTarget[jints];
Print[StringTemplate["BlackBoxAMFlow: target integrals split into in `1` parts."][ToStringInput[Length[targets]]]];
sol = Join@@Table[
Print[StringTemplate["BlackBoxAMFlow: computing part `1`."][ToStringInput[i]]];
sol = BlackBoxAMFlowSingle[targets[[i]], epslist, dir];
Print[StringTemplate["BlackBoxAMFlow: part `1` done."][ToStringInput[i]]];
sol, {i, Length[targets]}];
timef = AbsoluteTime[];
Print[StringTemplate["BlackBoxAMFlow: finished in `1`s."][ToStringInput[Ceiling[timef-times]]]];
sol
];


(* ::Subsubsection::Closed:: *)
(*application 1*)


GenerateNumericalConfig[goal_, order_]:=Block[{loop,number,eps0,epslist,singlepre,workpre,xorder},
loop = Max[Length[Loop], 1];
number = Ceiling[5/2*order+2*loop];
eps0 = Power[10, -1/2*loop-goal*(order+1)^-1];
eps0 = Rationalize[N[eps0, MachinePrecision], eps0/100];
If[number>100, Print["GenerateNumericalConfig: the given order is too large to evaluate."]; Abort[]];
epslist = eps0+Range[number]*eps0/100;
singlepre = Max[Ceiling[(number+2*loop)*(1/2*loop+goal*(order+1)^-1)], 30];
workpre = 2*singlepre;
xorder = 4*singlepre;
{epslist, workpre, xorder}
];


FitEps[epslist_, vlist_, leading_]:=Block[{$MinPrecision = $WorkingPre, $MaxPrecision = $WorkingPre, data},
data = GeneralizedTranspose[N[{epslist, vlist}, $WorkingPre]];
Chop[Fit[data, Power[$Eps, leading+Range[0, Length[epslist]-1]], $Eps], 10^-$ChopPre]
];


SolveIntegrals[jints_, goal_, order_]:=Block[{time1,epslist0,$WorkingPre,$XOrder,epslist,sol,leading,time2},
Print["SolveIntegrals: please note that the option value of \"WorkingPre\" and \"XOrder\" will not affect this function at all."];
Print["SolveIntegrals: started."];
time1 = AbsoluteTime[];

If[MemberQ[Keys[Numeric], $Eps], 

Print["SolveIntegrals: eps has been set to a number, only numerical results of integrals will be returned."]; 
epslist0 = {$Eps/.Numeric};
epslist = epslist0+(4-$D0)*1/2;
$WorkingPre = 2*goal;
$XOrder = 4*goal;
Print[StringTemplate["SolveIntegrals: integrals will be evaluated at 1 eps value `1`."][ToStringInput[epslist]]];
Print[StringTemplate["SolveIntegrals: working precision is `1` and truncated order is `2`."][ToStringInput[$WorkingPre], ToStringInput[$XOrder]]];
sol = BlackBoxAMFlow[jints, epslist];
sol = Thread[Keys[sol] -> Values[sol][[All, 1]]],

{epslist0, $WorkingPre, $XOrder} = GenerateNumericalConfig[goal, order];
epslist = epslist0+(4-$D0)*1/2;
Print[StringTemplate["SolveIntegrals: integrals will be evaluated at `1` different eps values `2`."][ToStringInput[Length[epslist]], ToStringInput[epslist]]];
Print[StringTemplate["SolveIntegrals: working precision is `1` and truncated order is `2`."][ToStringInput[$WorkingPre], ToStringInput[$XOrder]]];
sol = BlackBoxAMFlow[jints, epslist];
leading = -2*Length[Loop];
sol = Thread[Keys[sol] -> N[Normal@Series[FitEps[epslist0, #, leading]&/@Values[sol], {$Eps, 0, order+leading}], goal]]];

time2 = AbsoluteTime[];
Print[StringTemplate["SolveIntegrals: finished in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
sol
];


(* ::Subsubsection:: *)
(*application 2*)


ExpandGaugeX[dir_]:=Module[{name,template,rule,time},
name = FileNameJoin[{dir, "solve"}];
template = StringToTemplate[{
"If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName//SetDirectory;
Get[`desolver`];
{masters, table} = Get[\"reduction\"];
{mastersde, variables, diffeq} = Get[\"diffeq\"];
{point, boundary} = Get[\"boundary\"];",
"sol[n_]:=Module[{tablecoe,de,bc,masterexp,allexp},
SetDefaultOptions[];
SetGlobalOptions[\"WorkingPre\" -> `wkpre`, \"ChopPre\" -> `cppre`];
SetExpansionOptions[\"XOrder\" -> `xorder`, \"LearnXOrder\" -> `lorder`, \"TestXOrder\" -> `torder`];
tablecoe = Values[table]/.{variables[[1]] -> eta, eps -> boundary[[n,1]]}//Factor;
de = diffeq[[1]]/.{variables[[1]] -> eta, eps -> boundary[[n,1]]}//Factor;
bc = boundary[[n,2]];
LoadSystem[n, de, bc, point[[2]]];
SolveAsyExp[n];
masterexp = masters/.Thread[mastersde -> AsyExp[n]];
allexp = PlusAsyExp@MapThread[TimesAsyExp, {#, masterexp}]&/@tablecoe;
PickZeroRuleS/@allexp];",
"LaunchKernels[`kernel`];
results = ParallelTable[sol[i], {i, Length[boundary]}, DistributedContexts -> All];
CloseKernels[];",
"Put[Thread[Keys[table] -> Transpose[results]], \"solution\"];",
"Quit[];"}];

rule = <|
"desolver" -> ToStringInput[$DESolverPath],
"wkpre" -> ToStringInput[$WorkingPre],
"cppre" -> ToStringInput[$ChopPre],
"xorder" -> ToStringInput[$XOrder],
"lorder" -> ToStringInput[$LearnXOrder],
"torder" -> ToStringInput[$TestXOrder],
"kernel" -> ToStringInput[NThread]
|>;

FileTemplateApply[template, rule, name<>".wl"];
Print["ExpandGaugeX: expanding integrals near 0."];
time = RunCommand[{$WolframPath, "-noprompt", "-script", name<>".wl"}, "log"->name<>".log"];
Print[StringTemplate["ExpandGaugeX: integrals expanded in `1`s."][ToStringInput[Ceiling[time]]]];
];


GenerateSquare[prop_, symbol_]:=Module[{coerule,linear,factor},
coerule = CoefficientRules[prop, Loop];
If[Max[Total/@Keys[coerule]]===2, Return[prop]];
If[Max[Total/@Keys[coerule]]===0, Print["GenerateSquare: this propagator seems to be a constant :)" -> prop]; Abort[]];
linear = FromCoefficientRules[Select[coerule, Total[Keys[#]]===1&], Loop];
factor = Select[FactorList[linear][[All,1]], Intersection[Variables[#], Loop]=!={} && Intersection[Variables[#], ReducedLeg]==={}&];
If[Length[factor]=!=1, Print["GenerateSquare: cannot identify this denominator" -> prop]; Abort[]];
factor[[1]]^2+prop*symbol^-1
];


SolveIntegralsGaugeLink[jints_, goal_, order_]:=Block[{time1,symbol,posi,dir,master,table,extra,masterde,vars,diffeq,epslist0,$WorkingPre,$XOrder,epslist,min,sol,fit,time2},
Print["SolveIntegralsGaugeLink: please note that the option value of \"WorkingPre\" and \"XOrder\" will not affect this function at all."];
Print["SolveIntegralsGaugeLink: started."];
time1 = AbsoluteTime[];
symbol = Symbol[ToStringInput[Family]<>"x"];
SaveGlobalVariables[symbol];
AMFlowInfo["Propagator"] = GenerateSquare[#, symbol]&/@ReducedPropagator;
posi = If[!FreeQ[ReducedPropagator[[#]], symbol], #, Nothing[]]&/@Range[Length[ReducedPropagator]];
If[posi==={}, 
Print["SolveIntegralsGaugeLink: nothing to do with the propagators. Use SolveIntegrals instead."]; 
Return[SolveIntegrals[jints, goal, order]]];

dir = CacheDirectory["solveint_gauge_asyexp"];
Print["SolveIntegralsGaugeLink: temporary family created."];
PrintGlobalVariables[];

{master, table} = BlackBoxReduce[jints, {}, CacheDirectory["solveint_gauge_reduce"]];
extra[jint_]:=Power[symbol, -Total[(List@@jint)[[posi+1]]]];
PutFile[{master, Thread[Keys[table] -> extra/@Keys[table]*Values[table]]}, FileNameJoin[{dir, "reduction"}]];

{masterde, vars, diffeq} = BlackBoxDiffeq[master, {symbol}, CacheDirectory["solveint_gauge_diffeq"]];
PutFile[{masterde, vars, diffeq}, FileNameJoin[{dir, "diffeq"}]];
If[!SubsetQ[masterde, master], Print["SolveIntegralsGaugeLink: some master integrals are not included in the differential equations."]; Abort[]];

{epslist0, $WorkingPre, $XOrder} = GenerateNumericalConfig[goal, order];
epslist = epslist0 + 1/2 * (4-$D0);
Print[StringTemplate["SolveIntegralsGaugeLink: integrals will be evaluated at `1` different eps values `2`."][ToStringInput[Length[epslist]], ToStringInput[epslist]]];
Print[StringTemplate["SolveIntegralsGaugeLink: working precision is `1` and truncated order is `2`."][ToStringInput[$WorkingPre], ToStringInput[$XOrder]]];

min = Block[{factors, zero, rationscale, polelist}, 
factors = FactorList[Times@@(diffeq/.$Eps -> epslist[[1]]//Denominator//Flatten)][[2;;,1]];
zero[poly_]:=If[#=!={}, symbol/.#, {}]&/@NSolve[poly==0, symbol, WorkingPrecision -> $WorkingPre];
rationscale = Power[10, -20];
polelist = Rationalize[Join@@(zero/@factors), rationscale];
Rationalize[Select[polelist, #=!=0&]//Abs, rationscale]//Min];

AMFlowInfo["Propagator"] = ReducedPropagator/.symbol -> 1/10*min;
sol = BlackBoxAMFlow[masterde, epslist, CacheDirectory["solveint_gauge_amflow"]];
If[!SubsetQ[Keys[sol], masterde], Print["SolveIntegralsGaugeLink: some master integrals have not been solved yet."]; Abort[]];
sol = Thread[epslist -> GeneralizedTranspose[masterde/.sol]];
PutFile[{symbol -> 1/10*min, sol}, FileNameJoin[{dir, "boundary"}]];
LoadGlobalVariables[symbol];

ExpandGaugeX[dir];
sol = GetFile[FileNameJoin[{dir, "solution"}]];
fit = Block[{$MinPrecision = $WorkingPre, $MaxPrecision = $WorkingPre, leading, jintsvalue},
leading = -2*Length[Loop];
jintsvalue = FitEps[epslist0, #, leading]&/@Values[sol];
Chop[Series[#, {$Eps, 0, leading + order}]&/@jintsvalue//Normal, 10^-$ChopPre]];

time2 = AbsoluteTime[];
Print[StringTemplate["SolveIntegralsGaugeLink: finished in `1`s."][ToStringInput[Ceiling[time2-time1]]]];
If[$DeleteBlackBoxDirectory===True, DeleteDir[dir]];
Join[Thread[Keys[sol] -> N[fit, goal]], Thread[Complement[jints, Keys[sol]] -> 0]]
];


(* ::Subsection::Closed:: *)
(*end*)


InitializePackage[];


End[];


EndPackage[];
