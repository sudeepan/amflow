(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "Blade"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = loopxloop;
AMFlowInfo["Loop"] = {l1, l2, q};
AMFlowInfo["Leg"] = {p1, p2};
AMFlowInfo["Conservation"] = {};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, (p1+p2)^2 -> s};
AMFlowInfo["Propagator"] = {l1^2, (l1+p1)^2, (l1+p1+p2)^2, (l1+q)^2, l2^2, (l2-p2)^2, (l2-p2-p1)^2, (l2-q)^2, q^2-msq, (p1+p2-q)^2-m2sq, (l1-l2)^2, (p1-q)^2};
AMFlowInfo["Numeric"] = {s -> 10, msq -> 1, m2sq -> 2/5};
AMFlowInfo["NThread"] = 4;


(*define phase-space integration by inserting "Prescription" and "Cut"*)
(*AMFlowInfo["Prescription"] should be a list with the same length as AMFlowInfo["Loop"], each element being 1, -1 or 0 specifying the Feynman prescription of the corresponding loop, where 1 means +i0, -1 means -i0 and 0 means insensitive to prescription*)
(*AMFlowInfo["Cut"] should be a list with the same length as AMFlowInfo["Propagator"], each element being 1 or 0 specifying whether the corresponding propagator is on cut or not*)
(*each propagator on cut represents an integrated final-state particle, see README.md and issue 1 on gitlab for more details*)
AMFlowInfo["Prescription"] = {1, -1, 0};
AMFlowInfo["Cut"] = {0,0,0,0,0,0,0,0,1,1,0,0};


(*SolveIntegrals: computes given integrals with given precision goal up to given eps order*)
(*returned is a list of replacement rules like {j1 \[Rule] v1, j2 \[Rule] v2, ...}, where j1, j2, ... are integrals and v1, v2, ... are their results*)
integrals = {j[loopxloop,0,1,1,1,1,0,1,1,1,1,0,0]};
precision = 20;
epsorder = 8;
sol1 = SolveIntegrals[integrals, precision, epsorder];
Put[sol1, FileNameJoin[{current, "sol1"}]];


(*evaluate again with totally opposite prescription*)
AMFlowInfo["Prescription"] = {-1, 1, 0};
sol2 = SolveIntegrals[integrals, precision, epsorder];
Put[sol2, FileNameJoin[{current, "sol2"}]];


(*the two solutions are complex conjugate of each other*)
Re@Coefficient[Values[sol1], eps, 0]-Re@Coefficient[Values[sol2], eps, 0]
Im@Coefficient[Values[sol1], eps, 0]+Im@Coefficient[Values[sol2], eps, 0]


Quit[];
