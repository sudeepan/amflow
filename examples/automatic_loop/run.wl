(* ::Package:: *)

(*load the package*)
current = If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{current, "..", "..", "AMFlow.m"}]];


(*set ibp reducer, could be "Blade", "FiniteFlow+LiteRed", "FIRE+LiteRed" or "Kira"*)
SetReductionOptions["IBPReducer" -> "FiniteFlow+LiteRed"];


(*configuration of the integral family*)
AMFlowInfo["Family"] = box1;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p4)^2};
AMFlowInfo["Numeric"] = {s -> 100, t -> -1};
AMFlowInfo["NThread"] = 4;


(*SolveIntegrals: computes given integrals with given precision goal up to given eps order*)
(*returned is a list of replacement rules like {j1 \[Rule] v1, j2 \[Rule] v2, ...}, where j1, j2, ... are integrals and v1, v2, ... are their results*)
integrals = {j[box1,1,0,1,0], j[box1,-2,1,1,1], j[box1,1,1,1,1]};
precision = 10;
epsorder = 3;
sol1 = SolveIntegrals[integrals, precision, epsorder];
Put[sol1, FileNameJoin[{current, "gen_fflow_sol1"}]];


(*configuration of another integral family*)
AMFlowInfo["Family"] = box2;
AMFlowInfo["Loop"] = {l};
AMFlowInfo["Leg"] = {p1, p2, p3, p4};
AMFlowInfo["Conservation"] = {p4 -> -p1-p2-p3};
AMFlowInfo["Replacement"] = {p1^2 -> 0, p2^2 -> 0, p3^2 -> 0, p4^2 -> 0, (p1+p2)^2 -> s, (p1+p3)^2 -> t};
AMFlowInfo["Propagator"] = {l^2, (l+p1)^2, (l+p1+p2)^2, (l+p1+p2+p3)^2};
AMFlowInfo["Numeric"] = {s -> 100, t -> -99};
AMFlowInfo["NThread"] = 4;


(*automatic computation*)
target2 = {j[box2, 1,1,1,1],j[box2, 1,1,0,1],j[box2, 1,-1,1,0]};
sol2 = SolveIntegrals[target2, precision, epsorder];
Put[sol2, FileNameJoin[{current, "gen_fflow_sol2"}]];


Quit[];
